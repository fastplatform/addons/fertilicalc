# FaST Platform / addons / fertilicalc

Additional module that implements Spanish (Castilla y Leon & Andalucia) services used to calculates Nitrogen, Phosphorus and Potassium requirements for a given fertilization plan depending on the soil type and available soil samples.

This repository includes the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

> The algorithm used is based on the Python version of [the FertiliCalc tool](http://www.uco.es/fitotecnia/fertilicalc.html) from [Francisco J. Villalobos](mailto:ag1vimaf@uco.es), originally licensed under GPLv3.

## Services

### Fertilization

- [fertilization/fertilicalc](services/fertilization/fertilicalc)
- [fertilization/fertilicalc-api-gateway](services/fertilization/fertilicalc_api_gateway)
