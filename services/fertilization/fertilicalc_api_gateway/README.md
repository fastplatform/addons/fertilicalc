# fertilization/fertilicalc-api-gateway

```fertilization/fertilicalc-api-gateway``` is a [Hasura](https://hasura.io/) service that exposes a single GraphQL endpoint built upon [fertilization/fertilicalc](../fertilicalc) service and its Postgres database. 

This service therefore exposes a GraphQL ontology that can be natively federated at a higher level into another GraphQL ontology. It instantly enables the modular aspect of FertiliCalc custom logic.

Hasura metadata is available in [metadata](metadata) directory and is used to start 
the service locally and is pushed to deployment environnements.

### Environment variables

- `HASURA_ADMIN_SECRET_KEY`:  Hasura admin secret key

## Development setup

### Prerequisites

- [Docker](https://www.docker.com/)

### Start the API Gateway

```bash
make start
```