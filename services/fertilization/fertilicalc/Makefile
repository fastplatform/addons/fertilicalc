# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

-include secrets.env
export $(shell sed 's/=.*//' secrets.env)
include service.env
export $(shell sed 's/=.*//' service.env)

start: ## 
	python3 manage.py runserver 0.0.0.0:8100

migrations: ## 
	python3 manage.py makemigrations

migrate: ## 
	python3 manage.py migrate

createsuperuser: ## 
	echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'tech@fastplatform.eu', 'azerty')" | python3 manage.py shell

start-postgres: ## 
	docker run --name postgres-fertilicalc \
		-e POSTGRES_USER=fast \
		-e POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) \
		-e POSTGRES_DB=fertilicalc \
		-p 5439:5432 \
		-h 0.0.0.0 \
		-v $(shell pwd)/data/dev/postgres-fertilicalc:/var/lib/postgresql/data \
		-d postgis/postgis:latest

stop-postgres: ## 
	docker stop postgres-fertilicalc
	docker rm postgres-fertilicalc

reset-postgres: ## 
	rm -rf ./data/dev/postgres-fertilicalc/*

init: ## 
	python run.py init --import-directory=../../../init --clear

bazel-run: ## 
	OIDC_ENABLED=False \
		bazel run :fertilicalc-run

bazel-run-with-oidc: ## 
	bazel run :fertilicalc-run

bazel-run-init: ## 
	bazel run :fertilicalc-run -- init $(shell pwd)/../../../init --clear

bazel-run-collectstatic: ## 
	bazel run :fertilicalc-run -- collectstatic

bazel-django-manage-check-deploy: ## 
	bazel run . -- check --deploy

bazel-build-image: ## 
	docker run -ti \
		-v $$(pwd)/../../../:/fertilicalc \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-w /fertilicalc/services/fertilization/fertilicalc \
		cnero/bazel:3.1.0-py3.7 \
		run :image

shell: ## 
	python3 manage.py shell
