from typing import List
from fertilicalc.input import *


class Plan:
    """Contains all the rotations of a fertilization plan"""

    def __init__(
        self, 
        rotations: List[Rotation], 
        soil: Soil, 
        soil_sample: SoilSample, 
        p_threshold: Threshold,
        k_threshold: Threshold,
        soil_effect: SoilEffect,
        efficiency_factor: EfficiencyFactor,
        max_p_rate: MaximumNutrientRate,
        max_k_rate: MaximumNutrientRate,
        n_equation_parameter: NitrogenEquationParameter,
        strategy: Strategy):

        self.rotations = rotations
        self.soil = soil
        self.soil_sample = soil_sample
        self.p_threshold = p_threshold
        self.k_threshold = k_threshold
        self.soil_effect = soil_effect
        self.efficiency_factor = efficiency_factor
        self.max_p_rate = max_p_rate
        self.max_k_rate = max_k_rate
        self.n_equation_parameter = n_equation_parameter
        self.strategy = strategy
        self._n_rotations = len(rotations)
    
        
    @classmethod    
    def from_dict(cls, content: dict):



        rotations = [
            Rotation(
                crop_yield=content["rotation"][idx]['crop_yield'],
                burn_residues=content["rotation"][idx]['burn_residues'],
                cv=content["rotation"][idx].get('cv', 0),
                crop_features=CropFeatures(**content["rotation"][idx]['crop_features'])
            )
            for idx in range(len(content["rotation"]))]

        soil = Soil(**content["soil"])
        soil_sample = SoilSample(**content["sample"])

        p_threshold = Threshold(**content["p_threshold"])
        k_threshold = Threshold(**content["k_threshold"])

        soil_effect = SoilEffect(**content["soil_effect"])
        efficiency_factor = EfficiencyFactor(**content["efficiency_factor"])

        max_p_rate = MaximumNutrientRate(**content["max_p_rate"])
        max_k_rate = MaximumNutrientRate(**content["max_k_rate"])

        n_equation_parameter = NitrogenEquationParameter(**content["n_equation_parameter"])

        strategy = Strategy(**content["strategy"])

        return cls(
            rotations = rotations, soil = soil, soil_sample = soil_sample,
            p_threshold = p_threshold, k_threshold = k_threshold, soil_effect = soil_effect,
            efficiency_factor = efficiency_factor, max_p_rate = max_p_rate,
            max_k_rate = max_k_rate, n_equation_parameter = n_equation_parameter,
            strategy = strategy)
 

    def __str__(self):
        return "\n".join([str(rotation) for rotation in self.rotations])
