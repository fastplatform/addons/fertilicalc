from fertilicalc.plan import *
from fertilicalc.result import *

crop_features_1 = CropFeatures("Faba bean dry", 'Vicia faba', 'Pulses', 'grain', 90.0, 3.7, 0.47, 1.2, 'straw', 85.0, 1.6, 0.2, 1.6, True, 3.0, 4.9, 35, 100, 0.76, 0.17, 0.2, 1.5, 0.6, None)
crop_features_2 = CropFeatures("Oats", 'Avena sativa', 'Cereals', 'grain', 91.0, 1.6, 0.36, 0.44, 'straw', 89.5, 0.7, 0.1, 2.3, 0, 1.5, 1.8, 40, 10, 0.082493126, 0.12373968800000001, 0.206232814, 0.028354649, 0.04253197400000001, 0.070886623)
rotation1 = Rotation(1, "Faba bean dry", 2000, 20, 0, crop_features_1)
rotation2 = Rotation(2, "Oats", 3000, 20, 0, crop_features_2)
soil = Soil(0.1, 0.9, "Sandy")
soil_sample = SoilSample(0, 0, 0, 80, 7, datetime.now())
p_threshold = Threshold("Sandy", "Maintenance", 10)
k_threshold = Threshold("Sandy", "Maintenance", 100)
soil_effect = SoilEffect("Sandy", 1.68)
efficiency_factor = EfficiencyFactor("Sandy", "Maintenance", 1.2)
max_p_rate = MaximumNutrientRate(100)
max_k_rate = MaximumNutrientRate(275)
nitrogen_equation_parameter = NitrogenEquationParameter(10, 10, 0, 0.2, 0.8, 0.8)
nmp = Plan([rotation1, rotation2], soil, soil_sample, p_threshold, k_threshold, soil_effect, efficiency_factor, max_p_rate, max_k_rate, nitrogen_equation_parameter, "Maintenance", 1, 0)


def test_get_fixation_rate_first_rotation():
    
    n_reco = NRecommendation(nmp, 0)
    actual = n_reco.get_fixation_rate(0)

    expected = 0.8

    assert actual == expected


def test_get_fixation_rate_second_rotation():
    
    n_reco = NRecommendation(nmp, 0)
    actual = n_reco.get_fixation_rate(1)

    expected = 0.0

    assert actual == expected


def test_get_n_target_first_rotation():

    n_reco = NRecommendation(nmp, 0)
    actual = n_reco.get_n_target(0)

    expected = 3.7

    assert actual == expected


def test_get_n_target_second_rotation():

    n_reco = NRecommendation(nmp, 0)
    actual = n_reco.get_n_target(1)

    expected = 1.6

    assert actual == expected


def test_compute_export_one():

    n_reco = NRecommendation(nmp, 0)
    actual_not_rounded = n_reco.compute_export(0.5, 0, 2, 3)
    actual = {
        k: round(v)
        for k, v in actual_not_rounded.items()
    }

    expected = {
        'harv_dm': round(1800.0), 
        'harvest': round(36.0), 
        'residues': round(100.2857142857143), 
        'total': round(136.2857142857143)}
    
    assert actual == expected


def test_compute_export_two():

    n_reco = NRecommendation(nmp, 0)
    actual_not_rounded = n_reco.compute_export(0.5, 1, 3, 2)
    actual = {
        k: round(v)
        for k, v in actual_not_rounded.items()
    }

    expected = {
        'harv_dm': round(2730.0), 
        'harvest': round(81.9), 
        'residues': round(81.89999999999999), 
        'total': round(163.8)}
    
    assert actual == expected


def test_compute_requirements_first_rotation():

    n_reco = NRecommendation(nmp, 0)
    actual = round(n_reco.compute_requirements(0))

    expected = 12

    assert actual == expected


def test_compute_requirements_second_rotation():

    n_reco = NRecommendation(nmp, 1)
    actual = round(n_reco.compute_requirements(1))

    expected = 15

    assert actual == expected