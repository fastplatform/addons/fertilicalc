from fertilicalc.plan import Plan
from fertilicalc.recommendation import Recommendation
from fertilicalc.stats.gauss import GAUSS_DECILE 


class NRecommendation(Recommendation):
    """Define how to obtain a nitrogen recommendation"""

    def __init__(self, plan: Plan, rotation_idx: int):

        Recommendation.__init__(self, plan, rotation_idx)
        self._nb_rotations = len(plan.rotations)
        self._requirements = NRecommendation.compute_requirements(self, rotation_idx)

    def _get_requirements(self):
        return self._requirements
    

    def get_fixation_rate(self, rotation_idx: int):
        """Compute symbiotic fixation according to crop type and quantity of soil organic matter"""

        n_fix_code = self.rotations[rotation_idx].crop_features.nfix_code
        if n_fix_code == 1:
            fixation = 80 if self.soil_sample.som < 3 else 50
            if self.rotations[rotation_idx].crop_features.plant_species_group == "FORAGE_LEGUME":
                fixation += 10
        else:
            fixation = 0
        return fixation / 100


    def get_n_target(self, rotation_idx: int):
        """We aim different nitrogen concentration according to the chosen strategy.
        If 'Sufficiency' or 'Build-up reduced' are chosen, we use the minimum 
        nitrogen concentration as defined by the experts.
        If 'Build-up maximum' or 'Maintenance' are chosen, we use the typical 
        nitrogen concentration as defined by the farmer or by the experts (if no farmer input)"""

        n_typical = self.rotations[rotation_idx].crop_features.n
        if self.strategy in ['SUFFICIENCY', 'REDUCED']:
            n_min = self.rotations[rotation_idx].crop_features.n_min
            return n_typical if n_min == None else n_min
        else:
            return n_typical
    

    def compute_export(self, percentile: float, rotation_idx: int, harv_conc: float, res_conc: float):
        """Compute total amount of potassium exported as a sum of nitrogen in harvest organ and nitrogen in residues not left in field"""

        rotation_data = self.rotations[rotation_idx]

        mean_yield = rotation_data.crop_yield
        percentile_yield = mean_yield * (1 + rotation_data.cv_yield / 100 \
            * GAUSS_DECILE[percentile*10])
        percentile_dry_matter = percentile_yield \
            * self.rotations[rotation_idx].crop_features.dry_matter / 100
        harvest_export = percentile_dry_matter * harv_conc / 100

        harvest_index = self.rotations[rotation_idx].crop_features.hi / 100
        residues_dm_rate = (1 - harvest_index) / (harvest_index)
        percentile_dm_residues = percentile_dry_matter * residues_dm_rate
        residues_dm = res_conc / 100
        residues_export = percentile_dm_residues * residues_dm


        return {
            "harv_dm": percentile_dry_matter,
            "harvest": harvest_export,
            "residues": residues_export,
            "total": harvest_export + residues_export
        }
    

    def compute_requirements(self, rotation_idx: int):
        """Compute Nitrogen requirements for a single rotation based 
        on the algorithm described by F. Villalobos"""
        
        previous_rotation_idx = (self._nb_rotations -1) if rotation_idx == 0 else rotation_idx - 1
        previous_rotation = self.rotations[previous_rotation_idx]
        current_crop = self.rotations[rotation_idx].crop_features
        previous_crop = self.rotations[previous_rotation_idx].crop_features

        n_target = NRecommendation.get_n_target(self, rotation_idx)
        previous_n_target = NRecommendation.get_n_target(self, previous_rotation_idx)

        exported_n = NRecommendation.compute_export(
            self=self, 
            percentile=0.5, 
            rotation_idx=rotation_idx, 
            harv_conc=n_target,
            res_conc=current_crop.res_n)
        
        previous_exported_n = NRecommendation.compute_export(
            self=self, 
            percentile=0.5, 
            rotation_idx=previous_rotation_idx, 
            harv_conc=previous_n_target,
            res_conc=previous_crop.res_n)
        
        previous_fixation = previous_crop.nfix_code
        mineralization_coef = 0.5 + 0.2 * (previous_fixation + self.tillage)

        n_for_crop = (1 + self.n_equation_parameter.f_nr) * exported_n["total"]
        n_mineralized = (1 - previous_rotation.burn_residues) * mineralization_coef * previous_crop.f_res /100 * previous_exported_n["residues"]
        n_in_roots = self.n_equation_parameter.f_nr * previous_exported_n["total"]
        base_rate = self.n_equation_parameter.n_end + n_for_crop - n_mineralized - n_in_roots - self.n_equation_parameter.n_other

        if current_crop.plant_species_group == "TREES":
            if (current_crop.res_n == 0) | (current_crop.res_n == None):
                harvest_index = current_crop.hi / 100
                residues_dm_rate = (1 - harvest_index) / (harvest_index)
                n_rate = exported_n["harv_dm"] * (n_target / 100 + \
                    self.n_equation_parameter.beta_pl * residues_dm_rate * 0.01 * (1 - 0.5 * \
                        (1 - previous_rotation.burn_residues) * previous_crop.f_res / 100)) / \
                            self.n_equation_parameter.efic
            else:
                n_rate = base_rate / (1 - self.n_equation_parameter.n_lost)
        else:
            if current_crop.nfix_code:
                n_fixation = n_for_crop * NRecommendation.get_fixation_rate(self, rotation_idx)
                n_rate = (base_rate - n_fixation) / (1 - self.n_equation_parameter.n_lost)
            else:
                n_rate = base_rate / (1 - self.n_equation_parameter.n_lost)
        
        return max(0, n_rate)
    
    requirements = property(_get_requirements)