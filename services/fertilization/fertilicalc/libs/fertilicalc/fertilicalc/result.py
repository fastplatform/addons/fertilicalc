from fertilicalc.nitrogen.algo import NRecommendation
from fertilicalc.phosphorus.algo import PRecommendation
from fertilicalc.potassium.algo import KRecommendation
from fertilicalc.plan import Plan


class RecommendationSet:
    """Set of recommendation, i.e. for each rotation"""

    def __init__(self, plan: Plan):
        self.plan = plan
        self._nb_rotations = len(self.plan.rotations)
        self._requirements = RecommendationSet._get_requirements(self)
    
    def _get_requirements(self):
        return [
            {
                "n": NRecommendation(self.plan, rotation_idx).requirements,
                "p": PRecommendation(self.plan, rotation_idx).requirements,
                "k": KRecommendation(self.plan, rotation_idx).requirements
            }
            for rotation_idx in range(self._nb_rotations)
        ]

    requirements = property(_get_requirements)