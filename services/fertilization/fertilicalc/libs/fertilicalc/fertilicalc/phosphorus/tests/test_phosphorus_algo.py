from fertilicalc.plan import *
from fertilicalc.result import *

crop_features_1 = CropFeatures("Faba bean dry", 'Vicia faba', 'Pulses', 'grain', 90.0, 3.7, 0.47, 1.2, 'straw', 85.0, 1.6, 0.2, 1.6, True, 3.0, 4.9, 35, 100, 0.76, 0.17, 0.2, 1.5, 0.6, None)
crop_features_2 = CropFeatures("Oats", 'Avena sativa', 'Cereals', 'grain', 91.0, 1.6, 0.36, 0.44, 'straw', 89.5, 0.7, 0.1, 2.3, 0, 1.5, 1.8, 40, 10, 0.082493126, 0.12373968800000001, 0.206232814, 0.028354649, 0.04253197400000001, 0.070886623)
rotation1 = Rotation(1, "Faba bean dry", 2000, 20, 0, crop_features_1)
rotation2 = Rotation(2, "Oats", 3000, 20, 0, crop_features_2)
soil = Soil(0.1, 0.9, "Sandy")
soil_sample = SoilSample(0, 0, 0, 80, 7, datetime.now())
p_threshold = Threshold("Sandy", "Maintenance", 10)
k_threshold = Threshold("Sandy", "Maintenance", 100)
soil_effect = SoilEffect("Sandy", 1.68)
efficiency_factor = EfficiencyFactor("Sandy", "Maintenance", 1.2)
max_p_rate = MaximumNutrientRate(100)
max_k_rate = MaximumNutrientRate(275)
nitrogen_equation_parameter = NitrogenEquationParameter(10, 10, 0, 0.2, 0.8, 0.8)
nmp = Plan([rotation1, rotation2], soil, soil_sample, p_threshold, k_threshold, soil_effect, efficiency_factor, max_p_rate, max_k_rate, nitrogen_equation_parameter, "Maintenance", 1, 0)


def test_compute_exported_p_first_rotation():
    
    p_reco = PRecommendation(nmp, 0)
    actual = int(round(p_reco.compute_exported_p()))

    expected = 11

    assert actual == expected


def test_compute_exported_p_second_rotation():

    p_reco = PRecommendation(nmp, 1)
    actual = int(round(p_reco.compute_exported_p()))

    expected = 17

    assert actual == expected


def test_commpute_full_equation_low_intercept():

    p_reco = PRecommendation(nmp, 0)
    actual = int(round(p_reco.commpute_full_equation(1)))

    expected = 51

    assert actual == expected


def test_commpute_full_equation_high_intercept():

    p_reco = PRecommendation(nmp, 0)
    actual = int(round(p_reco.commpute_full_equation(10)))

    expected = 60

    assert actual == expected


def test_correct_threshold():

    p_reco = PRecommendation(nmp, 0)
    actual = int(round(p_reco.correct_threshold()))

    expected = 10

    assert actual == expected


def test_correct_threshold_high_yield_crops():

    rotation1 = Rotation(1, "Faba bean dry", 20000, 20, 0, crop_features_1)
    nmp = Plan(
        [rotation1, rotation2], soil, soil_sample, p_threshold, k_threshold, 
        soil_effect, efficiency_factor, max_p_rate, max_k_rate, 
        nitrogen_equation_parameter, "Maintenance", 1, 0)


    p_reco = PRecommendation(nmp, 0)
    actual = int(round(p_reco.correct_threshold()))

    expected = 20

    assert actual == expected


def test_compute_requirements():

    p_reco = PRecommendation(nmp, 0)
    actual = int(round(p_reco.compute_requirements()))

    expected = 11

    assert actual == expected


def test_compute_requirements_high_yield():

    rotation1 = Rotation(1, "Faba bean dry", 20000, 20, 0, crop_features_1)
    nmp = Plan( [rotation1, rotation2], soil, soil_sample, p_threshold, k_threshold,   soil_effect, efficiency_factor, max_p_rate, max_k_rate,   nitrogen_equation_parameter, "Maintenance", 1, 0)

    p_reco = PRecommendation(nmp, 0)
    actual = int(round(p_reco.compute_requirements()))

    expected = 106

    assert actual == expected