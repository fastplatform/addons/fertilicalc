from fertilicalc.recommendation import Recommendation
from fertilicalc.input import Rotation, SoilSample, Soil, Threshold
from fertilicalc.plan import Plan
from fertilicalc.stats.gauss import GAUSS_DECILE 


class PRecommendation(Recommendation):
    """Define how to obtain a phosphorus recommendation"""

    def __init__(self, plan: Plan, rotation_idx: int):
        Recommendation.__init__(self, plan, rotation_idx)
        self._requirements = PRecommendation.compute_requirements(self)

    def _get_requirements(self):
        return self._requirements

    def get_p_gap_in_soil(self):
        """Compute gap between phosphorus concentration and phosphorus threshold"""
        return max(0, self._corrected_threshold - self.soil_sample.p_conc)

    def compute_exported_p(self):
        """Compute total amount of potassium exported as a sum of potassium in harvest organ and potassium in residues not left in field"""

        rotation = self.plan.rotations[self.rotation_idx]

        percentile_90_yield = rotation.crop_yield * (1 + rotation.cv_yield / 100 * GAUSS_DECILE[9])
        percentile_90_dry_matter = percentile_90_yield * self.rotations[self.rotation_idx].crop_features.dry_matter / 100
        harvest_export = percentile_90_dry_matter * self.rotations[self.rotation_idx].crop_features.p / 100

        residues_dm_rate = (1 - self.rotations[self.rotation_idx].crop_features.hi / 100) / (self.rotations[self.rotation_idx].crop_features.hi / 100)
        percentile_90_dm_residues = percentile_90_dry_matter * residues_dm_rate
        p_residues_dm = self.rotations[self.rotation_idx].crop_features.res_p / 100
        p_residues_dm = 0.001 if p_residues_dm == 0 else p_residues_dm
        residues_export = percentile_90_dm_residues * p_residues_dm * (1 - self.rotations[self.rotation_idx].crop_features.f_res / 100)

        return harvest_export + residues_export

    def commpute_full_equation(self, intercept):
        b_coef_reducer = 1
        if intercept > self.max_p_rate.rate:
            p_rate = self.max_p_rate.rate
        else:
            while "P rate above maximum P rate":
                b_coef = 10 * self.soil_effect.coeff * 0.3 / b_coef_reducer
                p_rate = intercept + b_coef * PRecommendation.get_p_gap_in_soil(self)
                if p_rate > self.max_p_rate.rate:
                    b_coef_reducer += 1
                else:
                    break
        return p_rate
    
    def correct_threshold(self):
        """Modify soil P threshold for high yield crops"""

        rotation = self.plan.rotations[self.rotation_idx]

        percentile_90_yield = rotation.crop_yield * (1 + rotation.cv_yield / 100 * GAUSS_DECILE[9])
        percentile_90_dry_matter = percentile_90_yield * self.rotations[self.rotation_idx].crop_features.dry_matter / 100
        harvest_export = percentile_90_dry_matter * self.rotations[self.rotation_idx].crop_features.p / 100
        coeff_threshold = (harvest_export - 10) / 30

        if coeff_threshold > 2:
            coeff_threshold = 2
        elif coeff_threshold < 1:
            coeff_threshold = 1
        self._corrected_threshold = coeff_threshold * self.p_threshold.value 
        return self._corrected_threshold

    def compute_requirements(self):
        """Compute phosphorus requirements based on the equation described by F. Villalobos

        P_rate = A + B * (STL_t - STL) with :
        
        A : An intercept
        B : A factor depending on soil properties where B varies for different soil P tests
        STL_t : Soil test level threshold
        STL : Soil test level
        """

        PRecommendation.correct_threshold(self)
        above_threshold = self.soil_sample.p_conc > self._corrected_threshold
        exported_p = PRecommendation.compute_exported_p(self)

        if self.strategy == "SUFFICIENCY":
            if above_threshold:
                p_rate = 0
            else:
                p_rate = PRecommendation.commpute_full_equation(self, 0)
        elif self.strategy == "MAINTENANCE":
            p_rate = exported_p
        else:
            if above_threshold:
                above_double_threshold = self.soil_sample.p_conc > (2 * self._corrected_threshold)
                p_rate = 0.5 * exported_p if above_double_threshold else exported_p
            else:
                p_rate = PRecommendation.commpute_full_equation(self, exported_p)
        return p_rate

    requirements = property(_get_requirements)