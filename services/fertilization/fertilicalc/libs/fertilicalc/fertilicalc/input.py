class Soil:
    """Meta-info about soil like soil type or soil content"""

    def __init__(self, soil_type: str):
        self.soil_type = soil_type


class SoilSample:
    """Results of soil sample analysis in a laboratory"""

    def __init__(
        self, p_conc: float, k_conc: float, 
        som: float = 2, cec: float = None, ph: float = None):
        """Init of soil sample

        Args:
            p_conc (float): Phosphorus concentration in soil.
            k_conc (float): Potassium concentration in soil.
            som (float, optional): Soil organic matter in percentage. 
                Between 0 and 100. Defaults to 2.
            cec (float, optional): Cation-exchange capacity. Defaults to None.
            ph (float, optional): pH, potential of hydrogen. 
                Between 0 and 14. Defaults to None.
        """
        self.p_conc = p_conc
        self.k_conc = k_conc
        self.som = 2 if som == 0 else som
        self.cec = cec
        self.ph = ph


class Threshold:
    """Soil level threshold used by P and K algorithm"""

    def __init__(self, value: int):
        """Init of threshold used by P and K algorithm.

        Args:
            value (int):  Value of the threshold.
        """
        self.value = value


class SoilEffect:
    """Soil type impacts 'b' coefficient use in equation for phosphorus 
    and potassium requirements. It can be interpreted as bulk density"""

    def __init__(self, coeff: float):
        self.coeff = coeff


class EfficiencyFactor:
    """Efficiency factor depends on strategy and soil type. It is used
    in potassium requirements equation"""

    def __init__(self, factor: float):
        self.factor = factor


class MaximumNutrientRate:
    """Maximum nutrient rate that can be apply (used for P and K requirements)"""

    def __init__(self, rate):
        self.rate = rate


class CropFeatures:
    """Define all the features associated to a crop like NPK concentration 
    in harvest organ and in residues, harvest index, fraction of residues left
    in field, ..."""

    def __init__(
        self, plant_species_group: str, harvest_product: str, dry_matter: float, 
        n: float, p: float, k: float, hi: int, f_res: int, 
        nfix_code: bool, n_min: float = None, n_max: float = None, 
        res_product: str = None, res_dry_matter: float = 0, 
        res_n: float = 0, res_p: float = 0, res_k: float = 0,
        ca: float = None, mg: float = None, s: float = None, 
        res_ca: float = None, res_mg: float = None, res_s: float = None):
        """Init of crop features

        Args:
            plant_species_group (str): [description]
            harvest_product (str): Harvest product name
            dry_matter (float): Quantity of dry matter
            n (float): Typical nitrogen concentration (%)
            p (float): Typical phosphorus concentration (%)
            k (float): Typical potassium concentration (%)
            hi (int): Harvest index (%)
            f_res (int): Fraction of residues left in field (%)
            nfix_code (bool): [description]
            n_min (float, optional): Minimal nitrogen concentration (%). 
                Defaults to None.
            n_max (float, optional): Maximal nitrogen concetration (%). 
                Defaults to None.
            res_product (str, optional): Residual product name. Defaults to None.
            res_dry_matter (float, optional): Quantity of dry matter in residues. 
                Defaults to 0. (%)
            res_n (float, optional): Typical nitrogen concentration in residues. 
                Defaults to 0. (%)
            res_p (float, optional): Typical phosphorus concentration in residues. 
                Defaults to 0. (%)
            res_k (float, optional): Typical potassium concentration in residues. 
                Defaults to 0. (%)
            ca (float, optional): Typical calcium concentration. Defaults to None. (%)
            mg (float, optional): Typical magnesium concentration. Defaults to None. (%)
            s (float, optional): Typical sulfur concentration. Defaults to None. (%)
            res_ca (float, optional): Typical calcium concentration in residues. 
                Defaults to None. (%)
            res_mg (float, optional): Typical magnesium concentration in residues. 
                Defaults to None. (%)
            res_s (float, optional): Typical sulfur concentration in residues. 
                Defaults to None. (%)
        """

        self.plant_species_group = plant_species_group
        self.harvest_product = harvest_product
        self.dry_matter = dry_matter
        self.n = n
        self.p = p
        self.k = k
        self.res_product = res_product
        self.res_dry_matter = 0 if res_dry_matter is None else res_dry_matter
        self.res_n = 0 if res_n is None else res_n
        self.res_p = 0 if res_p is None else res_p
        self.res_k = 0 if res_k is None else res_k
        self.nfix_code = nfix_code
        self.n_min = n_min
        self.n_max = n_max
        self.hi = hi if (hi > 0) else 0.01
        self.f_res = f_res
        self.ca = ca
        self.mg = mg
        self.s = s
        self.res_ca = res_ca
        self.res_mg = res_mg
        self.res_s = res_s


class NitrogenEquationParameter:
    """Contains parameter of nitrogen equation"""

    def __init__(self, n_end: float, n_other: float, n_lost: float, f_nr, beta_pl, efic):
        """Init of nitrogen equation parameter

        Args:
            n_end (float): Final soil inorganic N
            n_other (float): N from atmospheric deposition, symbiotic fixation 
                and irrigation water
            n_lost (float): Fraction of N lost (leaching, volatilization, 
                denittrification) [between 0 and 1]
            f_nr (float): N in roots / N in shoots ratio [between 0 and 1]
            beta_pl (float): [description]
            efic (float): [description]
        """
        self.n_end = n_end
        self.n_other = n_other
        self.n_lost = n_lost if (n_lost < 1) else 0.99 
        self.f_nr = f_nr
        self.beta_pl = beta_pl
        self.efic = efic if efic > 0 else 0.01
        

class Rotation:
    """Contains attributes of a single crop rotation"""

    def __init__(
        self, 
        crop_yield: int, 
        burn_residues: bool,
        crop_features: CropFeatures,
        cv: float = 0
        ):

        self.crop_yield = crop_yield
        self.cv_yield = cv
        self.burn_residues = burn_residues
        self.crop_features = crop_features

    
    def __str__(self):
        return "Yield : {0} kg/ha (CV {1}%)".format(
            self.crop_yield, 
            self.cv_yield)


class Strategy:
    """Contains strategy, tillage and irrigation for all crops in a plan"""

    def __init__(
        self, 
        strategy: str, 
        tillage: bool):

        self.strategy = strategy
        self.tillage = tillage
    
    def __str__(self):
        return "{0} {1}".format(
            self.strategy, 
            "with tillage" if self.tillage else "without tillage")