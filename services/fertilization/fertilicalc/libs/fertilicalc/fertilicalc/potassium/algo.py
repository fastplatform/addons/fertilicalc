from fertilicalc.recommendation import Recommendation
from fertilicalc.input import Rotation, SoilSample, Soil, Threshold
from fertilicalc.plan import Plan
from fertilicalc.stats.gauss import GAUSS_DECILE 


class KRecommendation(Recommendation):
    """Define how to obtain a potassium recommendation"""

    def __init__(self, plan: Plan, rotation_idx: int):
        Recommendation.__init__(self, plan, rotation_idx)
        self._requirements = KRecommendation.compute_requirements(self)

    def _get_requirements(self):
        return self._requirements

    def get_k_gap_in_soil(self):
        """Compute gap between potassium concentration and potassium threshold"""
        return max(0, self.k_threshold.value - self.soil_sample.k_conc)

    def compute_exported_k(self):
        """Compute total amount of potassium exported as a sum of potassium in harvest organ and potassium in residues not left in field"""

        rotation = self.plan.rotations[self.rotation_idx]

        percentile_90_yield = rotation.crop_yield * (1 + rotation.cv_yield / 100 * GAUSS_DECILE[9])
        percentile_90_dry_matter = percentile_90_yield * self.rotations[self.rotation_idx].crop_features.dry_matter / 100
        harvest_export = percentile_90_dry_matter * self.rotations[self.rotation_idx].crop_features.k / 100

        residues_dm_rate = (1 - self.rotations[self.rotation_idx].crop_features.hi / 100) / (self.rotations[self.rotation_idx].crop_features.hi / 100)
        percentile_90_dm_residues = percentile_90_dry_matter * residues_dm_rate
        k_residues_dm = self.rotations[self.rotation_idx].crop_features.res_k / 100
        k_residues_dm = 0.001 if k_residues_dm == 0 else k_residues_dm
        residues_export = percentile_90_dm_residues * k_residues_dm * (1 - self.rotations[self.rotation_idx].crop_features.f_res / 100)

        return harvest_export + residues_export

    def commpute_full_equation(self, intercept):
        b_coef_reducer = 1
        if intercept > self.max_k_rate.rate:
            k_rate = self.max_k_rate.rate
        else:
            while "K rate above maximum K rate":
                b_coef = 10 * self.soil_effect.coeff * 0.3 / b_coef_reducer
                k_rate = intercept + b_coef * self.efficiency_factor.factor * KRecommendation.get_k_gap_in_soil(self)
                if k_rate > self.max_k_rate.rate:
                    b_coef_reducer += 1
                else:
                    break
        return k_rate


    def compute_requirements(self):
        """Compute potassium requirements based on the equation described by F. Villalobos

        K_rate = A + (f_k * B) * (STL_t - STL) with :
        
        A : An intercept
        f_k : An efficiency factor depending on the clay content of soil
        B : A factor depending on soil properties where B is the same for different soil K tests
        STL_t : Soil test level threshold
        STL : Soil test level
        """

        above_threshold = self.soil_sample.k_conc >= self.k_threshold.value
        strictly_above_threshold = self.soil_sample.k_conc > self.k_threshold.value
        exported_k = KRecommendation.compute_exported_k(self)

        if self.strategy == "SUFFICIENCY":
            if strictly_above_threshold:
                k_rate = 0
            else:
                k_rate = KRecommendation.commpute_full_equation(self, 0)
        elif self.strategy == "MAINTENANCE":
            k_rate = exported_k
        else:
            if above_threshold:
                above_double_threshold = self.soil_sample.k_conc >= (2 * self.k_threshold.value)
                k_rate = 0.5 * exported_k if above_double_threshold else exported_k
            else:
                k_rate = KRecommendation.commpute_full_equation(self, exported_k)
        return k_rate

    requirements = property(_get_requirements)