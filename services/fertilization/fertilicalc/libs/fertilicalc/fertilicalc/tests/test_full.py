import json
import os
import pytest

from fertilicalc.plan import Plan
from fertilicalc.result import RecommendationSet
from fertilicalc.input import (
    Soil, Rotation, CropFeatures, SoilEffect, SoilSample, MaximumNutrientRate,
    Threshold, EfficiencyFactor, NitrogenEquationParameter, Strategy
)

PATH = os.path.dirname(os.path.abspath(__file__))
JSON_PATH = PATH + '/fertilicalc_result.json'

ABSOLUTE_APPROX = 1

with open(JSON_PATH) as jsonfile:
    fertilicalc_data = json.load(jsonfile)

def assert_each_npk_almost_equal(actual, expected, abs=1):
    for idx_rotation in range(len(expected)):
        assert pytest.approx(actual[idx_rotation], abs=abs) == expected[idx_rotation]

def input_to_recommendations(input_algo):

    nmp = Plan.from_dict(input_algo)

    return RecommendationSet(nmp)

def round_requirements(requirements):

    return [
        {key: int(round(val)) for key, val in rotation.items()}
        for rotation in requirements
    ]


def test_json_0():

    toy_data = fertilicalc_data[0]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_1():

    toy_data = fertilicalc_data[1]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_2():

    toy_data = fertilicalc_data[2]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_3():

    toy_data = fertilicalc_data[3]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_4():

    toy_data = fertilicalc_data[4]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_5():

    toy_data = fertilicalc_data[5]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_6():

    toy_data = fertilicalc_data[6]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_7():

    toy_data = fertilicalc_data[7]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_8():

    toy_data = fertilicalc_data[8]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_9():

    toy_data = fertilicalc_data[9]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_10():

    toy_data = fertilicalc_data[10]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_11():

    toy_data = fertilicalc_data[11]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_12():

    toy_data = fertilicalc_data[12]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)


def test_json_13():

    toy_data = fertilicalc_data[13]
    
    expected = toy_data['output']

    recommendation = input_to_recommendations(toy_data['input'])
    print(recommendation.requirements)
    actual = round_requirements(recommendation.requirements)

    assert_each_npk_almost_equal(actual, expected, ABSOLUTE_APPROX)
