from fertilicalc.input import Rotation, Soil, SoilSample
from fertilicalc.plan import Plan


class Recommendation:
    """Abstract class for the N, P and K recommendations"""

    def __init__(self, plan: Plan, rotation_idx: int):
        self.plan = plan
        self.rotations  = plan.rotations
        self.soil = plan.soil
        self.soil_sample = plan.soil_sample
        self.p_threshold = plan.p_threshold
        self.k_threshold = plan.k_threshold
        self.soil_effect = plan.soil_effect
        self.efficiency_factor = plan.efficiency_factor
        self.max_p_rate = plan.max_p_rate
        self.max_k_rate = plan.max_k_rate
        self.n_equation_parameter = plan.n_equation_parameter
        self.strategy = plan.strategy.strategy
        self.tillage = plan.strategy.tillage
        self.rotation_idx = rotation_idx
