from constance.apps import ConstanceConfig
from django.utils.translation import ugettext_lazy as _


class CustomConstance(ConstanceConfig):
    verbose_name = _('Parameters')