from mozilla_django_oidc.auth import OIDCAuthenticationBackend

class MyOIDCAB(OIDCAuthenticationBackend):
    def create_user(self, claims):
        username = claims.get('nickname')
        user = self.UserModel.objects.create_user(username=username)

        if 'admin' in claims['groups']:
            user.is_staff = True
            user.is_superuser = True
            
        user.save()

        return user


    def update_user(self, user, claims):
        user.username = claims.get('nickname')

        if 'admin' in claims['groups']:
            user.is_staff = True
            user.is_superuser = True

        user.save()

        return user

    def filter_users_by_claims(self, claims):
        """Return all users matching the specified email."""
        username = claims.get('nickname')
        if not username:
            return self.UserModel.objects.none()
        return self.UserModel.objects.filter(username__iexact=username)
