"""
Django settings for backend project.

Generated by 'django-admin startproject' using Django 3.0.4.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import os
from collections import OrderedDict
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('DEBUG', 'TRUE').upper() == 'TRUE'

DJANGO_ALLOWED_HOSTS = os.environ.get('DJANGO_ALLOWED_HOSTS', 'localhost.fastplatform.eu,localhost,host.docker.internal,172.17.0.1')

ALLOWED_HOSTS = [] + DJANGO_ALLOWED_HOSTS.split(',')

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'backend.apps.CustomConstance',
    'constance.backends.database',
    'api.apps.ApiConfig',
    'mozilla_django_oidc',
    'storages'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'backend.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_DATABASE', 'fertilicalc'),
        'USER': os.environ.get('POSTGRES_USER', 'fast'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': os.environ.get('POSTGRES_HOST', 'localhost'),
        'PORT': os.environ.get('POSTGRES_PORT', '5439'),
        "CONN_MAX_AGE": int(os.environ.get("POSTGRES_CONN_MAX_AGE", 0)),
        "DISABLE_SERVER_SIDE_CURSORS": os.environ.get("DISABLE_SERVER_SIDE_CURSORS", "TRUE").upper() == "TRUE",
    },
}


# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# S3 storage
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html

AWS_ACCESS_KEY_ID = os.environ.get("DJANGO_AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.environ.get("DJANGO_AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = os.environ.get("DJANGO_AWS_STORAGE_BUCKET_NAME")
AWS_S3_ENDPOINT_URL = os.environ.get("DJANGO_AWS_S3_ENDPOINT_URL")
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': "max-age={}".format(
        os.environ.get("DJANGO_AWS_S3_OBJECT_PARAMETERS_MAX_AGE", '86400')
    ),
}
AWS_LOCATION = os.environ.get("DJANGO_AWS_LOCATION")
AWS_QUERYSTRING_AUTH = os.environ.get('DJANGO_AWS_QUERYSTRING_AUTH', 'FALSE').upper() == 'TRUE'
AWS_IS_GZIPPED = os.environ.get('DJANGO_AWS_IS_GZIPPED', 'TRUE').upper() == 'TRUE'
AWS_DEFAULT_ACL = None

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATICFILES_STORAGE = os.environ.get(
    "DJANGO_STATICFILES_STORAGE", 
    'django.contrib.staticfiles.storage.StaticFilesStorage')
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
CONSTANCE_IGNORE_ADMIN_VERSION_CHECK = True
CONSTANCE_CONFIG = OrderedDict([
    ("MAX_P_RATE", (100, _('Maximum phosphorus rate'), int)),
    ("MAX_K_RATE", (275, _('Maximum potassium rate'), int)),
    ('N_END', (10, _('Final soil inorganic N'), int)),
    ('N_OTHER', (10, _('N from atmospheric deposition, symbiotic fixation and irrigation water'), int)),
    ('N_LOST', (0.0, _('Fraction of N lost (leaching, volatilization, denittrification) [between 0 and 1]'), float)),
    ('F_NR', (0.2, _('N in roots / N in shoots ratio [between 0 and 1]'), float)),
    ('BETA_PL', (0.8, '', float)),
    ('EFIC', (0.8, '', float)),
])

CSRF_COOKIE_SECURE = os.environ.get("DJANGO_CSRF_COOKIE_SECURE", "FALSE").upper() == "TRUE"

SECURE_HSTS_SECONDS = int(os.environ.get("DJANGO_SECURE_HSTS_SECONDS", 0))
if SECURE_HSTS_SECONDS > 0:
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True

SECURE_REFERRER_POLICY = 'same-origin'

SESSION_COOKIE_SECURE = os.environ.get("DJANGO_SESSION_COOKIE_SECURE", "FALSE").upper() == "TRUE"

# AUTHENTICATION - OIDC
OIDC_ENABLED = os.environ.get('OIDC_ENABLED', "True").upper() == "TRUE"

if OIDC_ENABLED:

    OIDC_RP_CLIENT_ID = os.environ.get('OIDC_RP_CLIENT_ID')
    OIDC_RP_CLIENT_SECRET = os.environ.get('OIDC_RP_CLIENT_SECRET')

    OIDC_RP_SIGN_ALGO = 'RS256'

    OP_URL = os.environ.get('OP_URL', 'https://localhost.fastplatform.eu:48000')

    OIDC_OP_URL = OP_URL + '/openid'

    # URL of the OIDC OP jwks endpoint
    OIDC_OP_JWKS_ENDPOINT = OIDC_OP_URL + "/jwks/"

    AUTHENTICATION_BACKENDS = (
        'backend.oidc_provider_settings.MyOIDCAB',
    )

    # URL of the OIDC OP authorization endpoint
    OIDC_OP_AUTHORIZATION_ENDPOINT = OIDC_OP_URL + "/authorize/"
    # URL of the OIDC OP token endpoint
    OIDC_OP_TOKEN_ENDPOINT = OIDC_OP_URL + "/token/"
    # URL of the OIDC OP userinfo endpoint
    OIDC_OP_USER_ENDPOINT = OIDC_OP_URL + "/userinfo/"

    OIDC_RP_SCOPES = "openid profile groups"

    LOGIN_URL = '/oidc/authenticate/'
    # URL path to redirect to after login
    LOGIN_REDIRECT_URL = "/admin/"
    # URL path to redirect to after logout
    LOGOUT_REDIRECT_URL = OP_URL + "/admin/logout"

#### PDF GENERATOR
PDF_GENERATOR_ENDPOINT = os.environ.get('PDF_GENERATOR_ENDPOINT', 'http://0.0.0.0:7010/generate_pdf')

#### ALGORITHM NAME
ALGORITHM_NAME = os.environ.get('ALGORITHM_NAME', 'FertiliCalc')
