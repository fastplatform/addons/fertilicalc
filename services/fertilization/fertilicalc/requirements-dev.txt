pylint==2.4.4
pylint-django==2.0.13
pylint-plugin-utils==0.6
mccabe==0.6.1
yapf==0.29.0
django_debug_toolbar==2.2
ptvsd==4.3.2