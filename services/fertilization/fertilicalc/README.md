# fertilization/fertilicalc

```fertilization/fertilicalc``` is a [Django](https://www.djangoproject.com/) service that exposes endpoints that calculates Nitrogen, Phosphorus and Potassium requirements for a given fertilization plan depending on the soil type and available soil samples.

## Prerequisites

- Python 3.7+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)

## Environment variables

- `ALGORITHM_NAME`: name of the algorithm as displayed on the fertilization plan report
- `API_GATEWAY_FASTPLATFORM_URL`: URL of the [fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/fastplatform) Hasura API Gateway
- `API_GATEWAY_SERVICE_KEY`: secret servive key to access [external](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/external) and [fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/fastplatform) Hasura API Gateways
- `DJANGO_SECRET_KEY`: Django secret key
- `LOG_LEVEL`: logging level (trace, debug, info, warn, error, fatal)
- `POSTGRES_DATABASE`: database name of FertiliCalc Postgres database
- `POSTGRES_HOST`: host of FertiliCalc Postgres database
- `POSTGRES_PORT`: port of FertiliCalc Postgres database
- `POSTGRES_USER`: user of FertiliCalc Postgres database
- `POSTGRES_PASSWORD`: password of FertiliCalc Postgres database

## Development Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```
make start-tracing
```
Jaeger UI will be available at [http://localhost:16686](http://localhost:16686)

Start the service:
```bash
make start-postgres # Start a local Postgres database
make migrations # Generate Django migrations
make migrate # Apply Django migrations
make init # Initialize the local Postgres database
make start
```

The API server is now started and available at [http://localhost:8100](http://localhost:8100).

**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run 
$ make bazel-run-init # Initialize the local Postgres database if needed (to be executed from an other shell)
```

## Sample 

```bash
curl --location
    --request POST 'localhost:8100/fertilicalc/algo/'
    --header 'Content-Type: application/json'
    --data-raw '{... payload ...}'
```

With the following payload:

```json
{
    "rotation": [
        {
            "crop_yield":1000,
            "cv":20,
            "burn_residues":20,
            "crop_features":         {
	            "plant_species_group": "PULSES",
	            "harvest_product": "grain",
	            "dry_matter": 90,
	            "n": 3.7,
	            "p": 0.47,
	            "k": 1.2,
	            "res_product": "straw",
	            "res_dry_matter": 85,
	            "res_n": 1.6,
	            "res_p": 0.2,
	            "res_k": 1.6,
	            "nfix_code": true,
	            "n_min": 3,
	            "n_max": 4.9,
	            "hi": 50,
	            "f_res": 100,
	            "ca": 0.76,
	            "mg": 0.17,
	            "s": 0.2,
	            "res_ca": 1.5,
	            "res_mg": 0.6,
	            "res_s": 0.1
	        }
        }
    ],
    "soil":{
        "soil_type":"SANDY"
    },
    "sample":{
        "p_conc":1.2,
        "k_conc":2.1,
        "som":1,
        "cec":80,
        "ph":7
    },
    "p_threshold":{
            "value": 10
    },
    "k_threshold":{
            "value": 100
    },
    "soil_effect":{
            "coeff": 1.68
    },
    "efficiency_factor":{
            "factor": 1.2
    },
    "max_p_rate": {
            "rate": 100
    },
    "max_k_rate": {
            "rate": 100
    },
    "n_equation_parameter": {
            "n_end": 10,
            "n_other": 10,
            "n_lost": 0,
            "f_nr": 0.2,
            "beta_pl": 0.8,
            "efic": 0.8
    },
    "strategy":{
        "strategy":"MAINTENANCE",
        "tillage":true
    }
}
```

You can find more details about inputs in file [`libs/fertilicalc/fertilicalc/input.py`](libs/fertilicalc/fertilicalc/input.py).


## Building Docker image

This repository use [Bazel](https://docs.bazel.build) Docker [rules](https://github.com/bazelbuild/rules_docker) to pull, build and push images.

Build a Docker image with the following target:
```shell
make bazel-build-image
```

Check your local registry:
```shell
docker images bazel/services/fertilization/fertilicalc:image
```

Make sure that you have a local Postgres instance running.

Run the FertiliCalc Docker image:

* On Linux

```shell
docker run -ti --rm \
    --name fertilicalc \
    --network host \
    -e DEBUG=TRUE \
    -e DJANGO_ALLOWED_HOSTS=localhost \
    -e DJANGO_SECRET_KEY=tochange \
    -e POSTGRES_HOST=localhost \
    -e POSTGRES_PORT=5432 \
    -e POSTGRES_DATABASE=fertilicalc \
    -e POSTGRES_USER=fast \
    -e POSTGRES_PASSWORD=tochange \
    -e OIDC_ENABLED=False \
    -v $(pwd)/../../../init:/init \
    bazel/services/fertilization/fertilicalc:image
```

* On OSX

```shell
docker run -ti --rm \
    --name fertilicalc \
    -p 8000:8000 \
    -e DEBUG=TRUE \
    -e DJANGO_ALLOWED_HOSTS=localhost \
    -e DJANGO_SECRET_KEY=tochange \
    -e POSTGRES_HOST=host.docker.internal \
    -e POSTGRES_PORT=5432 \
    -e POSTGRES_DATABASE=fertilicalc \
    -e POSTGRES_USER=fast \
    -e POSTGRES_PASSWORD=tochange \
    -e OIDC_ENABLED=False \
    -v $(pwd)/../../../init:/init \
    bazel/services/fertilization/fertilicalc:image
```

Initialize the database:
```shell
docker exec -ti fertilicalc sh -c 'python ../../*.binary init /init --clear'
```

Service is available at [http://localhost:8000](http://localhost:8000).
