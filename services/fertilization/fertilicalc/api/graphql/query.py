import os


CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
QUERY_DIRNAME = "gql_query"
QUERY_PATH = CURRENT_DIR + '/' + QUERY_DIRNAME


def get_chemical_element_percentage_and_efficiency(
    registered_fertilizer_elements: [dict], 
    chemical_element_id: str
) -> float:
    """Return the percentage of a chemical element among the registered 
        elements of a fertilizer

    Args:
        registered_fertilizer_elements ([dict]): [
            {
                'chemical_element_id': 'N', 
                'percentage': 20, 
                'efficiency': 100
            }, {
                'chemical_element_id': 'P', 
                'percentage': 4.4, 
                'efficiency': 100
            }, {
                'chemical_element_id': 'K', 
                'percentage': 4.15, 
                'efficiency': 100
            }
        ]
        chemical_element_id (str): ID of the chemical element : 'N', 'P' or 'K'

    Returns:
        float: Concentration in percentage for this chemical element
    """
    element = list(filter(
        lambda x: x['chemical_element_id'] == chemical_element_id, 
        registered_fertilizer_elements
    ))
    percentage = element[0]['percentage'] / 100 if element else 0
    efficiency = element[0]['efficiency'] / 100 if element else 0

    return {'percentage': percentage, 'efficiency': efficiency}


def get_all_registered_compound_fertilizer(hasura_client):

    gql_file = f'{QUERY_PATH}/get_all_registered_fertilizer.graphql'
    with open(gql_file, 'r') as file:
        get_fertilizer = file.read()
    
    all_registered_fertilizer = hasura_client.execute(get_fertilizer)['registered_fertilizer']

    all_registered_compound_fertilizer = list(filter(
        lambda x: is_compound_fertilizer(x),
        all_registered_fertilizer
    ))

    return all_registered_compound_fertilizer


def get_registered_compound_fertilizer_with_n(hasura_client):

    gql_file = f'{QUERY_PATH}/get_all_registered_fertilizer.graphql'
    with open(gql_file, 'r') as file:
        get_fertilizer = file.read()
    
    all_registered_fertilizer = hasura_client.execute(get_fertilizer)['registered_fertilizer']

    registered_compound_fertilizer_with_n = list(filter(
        lambda x: is_compound_fertilizer_with_n(x),
        all_registered_fertilizer
    ))

    print(registered_compound_fertilizer_with_n)

    return registered_compound_fertilizer_with_n


def is_compound_fertilizer(fertilizer):

    fertilizer_elements = fertilizer['registered_fertilizer_elements']
    n_percentage, p_percentage, k_percentage = [
        get_chemical_element_percentage_and_efficiency(
            registered_fertilizer_elements=fertilizer_elements,
            chemical_element_id=chemical_element_id
        )['percentage']
        for chemical_element_id in ['N', 'P', 'K']
    ]

    nb_element_in_fertilizer = (n_percentage > 0) + \
                               (p_percentage > 0) + \
                               (k_percentage > 0)

    return True if nb_element_in_fertilizer > 1 else False


def is_compound_fertilizer_with_n(fertilizer):

    fertilizer_elements = fertilizer['registered_fertilizer_elements']
    n_percentage, p_percentage, k_percentage = [
        get_chemical_element_percentage_and_efficiency(
            registered_fertilizer_elements=fertilizer_elements,
            chemical_element_id=chemical_element_id
        )['percentage']
        for chemical_element_id in ['N', 'P', 'K']
    ]

    nb_element_in_fertilizer = (n_percentage > 0) + \
                               (p_percentage > 0) + \
                               (k_percentage > 0)

    return True if (nb_element_in_fertilizer > 1) & (n_percentage > 0) else False
