from api.graphql.query import (get_all_registered_compound_fertilizer,
                               get_registered_compound_fertilizer_with_n)
from fastplatform_graphql.hasura import HasuraClient
from api.graphql.query import get_chemical_element_percentage_and_efficiency


def get_best_compound_fertilizer_with_topdressing(hasura: HasuraClient, 
                                 n: float, 
                                 p: float, 
                                 k: float) -> dict:
    """Get best compound fertilizer for given NPK needs.

    The selection process is performed in two steps :
        1 - From a list of fertilizer, we choose the 2 products with the best 
            (P, K) equilibrium.
        2 - From the 2 products selected at first step, we keep the fertilizer 
            which has the closest N share.

    Args:
        hasura (HasuraClient): An Hasura client from fastplatform_graphql lib
        n (float): Quantity of nitrogen in kg/ha.
        p (float): Quantity of phosphorus in kg/ha.
        k (float): Quantity of potassium in kg/ha.

    Returns:
        dict: Contains the id and the name of the best compound fertilizer.
    """

    all_registered_compound_fertilizer = get_all_registered_compound_fertilizer(
        hasura_client=hasura
    )

    first_step_fertilizers = get_fertilizers_with_best_pk_equilibrium(
        p=p, k=k, fertilizers=all_registered_compound_fertilizer
    )

    best_fertilizer = get_fertilizer_with_closest_n_share(
        n=n, p=p, k=k, fertilizers=first_step_fertilizers
    )

    fertilizer_quantity = compute_fertilizer_quantity_with_topdressing(
        p=p, k=k, fertilizer=best_fertilizer
    )

    flattened_fertilizer_quantity = flatten_fertilizer(fertilizer_quantity)

    useful_keys = [
        'id', 'name', 'fertilizer_type_id', 'quantity', 
        'n_percentage', 'n_efficiency', 'p_percentage', 'p_efficiency',
        'k_percentage', 'k_efficiency', 'n_kg', 'p_kg', 'k_kg'
    ]

    return {key: flattened_fertilizer_quantity[key] for key in useful_keys}


def get_best_compound_fertilizer_without_topdressing(
    hasura: HasuraClient, 
    n: float, 
    p: float, 
    k: float
) -> dict:
    """Get best compound fertilizer for given NPK needs without todressing.

    The selection process is performed in two steps :
        1 - From a list of fertilizer, we choose the 2 products with the best 
            (P, K) equilibrium.
        2 - From the 2 products selected at first step, we keep the fertilizer 
            which has the closest N share.

    Args:
        hasura (HasuraClient): An Hasura client from fastplatform_graphql lib
        n (float): Quantity of nitrogen in kg/ha.
        p (float): Quantity of phosphorus in kg/ha.
        k (float): Quantity of potassium in kg/ha.

    Returns:
        dict: Contains the id and the name of the best compound fertilizer.
    """

    total_npk = n + p + k
    target_n_ratio = n / total_npk
    target_p_ratio = p / total_npk
    target_k_ratio = k / total_npk

    if target_n_ratio == 0:
        return get_best_compound_fertilizer_with_topdressing(hasura, n, p, k)
    else:

        all_compound_fertilizer_with_n = get_registered_compound_fertilizer_with_n(
            hasura_client=hasura
        )

        best_fertilizer = get_fertilizer_with_best_npk_equilibrium(
            n=target_n_ratio,
            p=target_p_ratio,
            k=target_k_ratio,
            fertilizers=all_compound_fertilizer_with_n
        )

        fertilizer_quantity = compute_fertilizer_quantity_without_topdressing(
            n=n, fertilizer=best_fertilizer
        )

        flattened_fertilizer_quantity = flatten_fertilizer(fertilizer_quantity)

        useful_keys = [
            'id', 'name', 'fertilizer_type_id', 'quantity', 
            'n_percentage', 'n_efficiency', 'p_percentage', 'p_efficiency',
            'k_percentage', 'k_efficiency', 'n_kg', 'p_kg', 'k_kg'
        ]

        return {key: flattened_fertilizer_quantity[key] for key in useful_keys}


def flatten_fertilizer(fertilizer: dict) -> dict:
    """Flatten a fertilizer with registered chemical elements 
        (for output of Hasura action)

    Args:
        fertilizer (dict): A fertilizer as dict with following schemas :
        {
            'id': 1,
            'name': '15-20-55',
            'registered_fertilizer_elements': [
                {
                    'chemical_element_id': 'N', 
                    'percentage': 20, 
                    'efficiency': 100
                }, {
                    'chemical_element_id': 'P', 
                    'percentage': 4.4, 
                    'efficiency': 100
                }, {
                    'chemical_element_id': 'K', 
                    'percentage': 4.15, 
                    'efficiency': 100
                }
            ]
        }

    Returns:
        dict: A flattened fertilizer dict. With the input dict in example, 
            result is :
            {
                'id': 1,
                'name': '15-20-55',
                'n_percentage': 0.2,
                'n_efficiency': 1,
                'p_percentage': 0.044,
                'p_efficiency': 1,
                'k_percentage': 0.0415,
                'k_efficiency': 1,
            }
    """

    for chemical_element in ['N', 'P', 'K']:

        percentage_and_efficiency = get_chemical_element_percentage_and_efficiency(
            registered_fertilizer_elements=fertilizer['registered_fertilizer_elements'],
            chemical_element_id=chemical_element
        )
        percentage = percentage_and_efficiency['percentage']
        efficiency = percentage_and_efficiency['efficiency']

        if chemical_element == 'N':
            fertilizer['n_percentage'] = percentage
            fertilizer['n_efficiency'] = efficiency
        elif chemical_element == 'P':
            fertilizer['p_percentage'] = percentage
            fertilizer['p_efficiency'] = efficiency
        elif chemical_element == 'K':
            fertilizer['k_percentage'] = percentage
            fertilizer['k_efficiency'] = efficiency
    
    del fertilizer['registered_fertilizer_elements']

    return fertilizer



def get_fertilizers_with_best_pk_equilibrium(p: float, 
                                             k: float, 
                                             fertilizers: [dict]) -> [dict]:
    """Return fertilizers with the two best pk equilibrium compared to a given 
        (p,k) couple.

    Args:
        p (float): Target P
        k (float): Target K
        fertilizers ([dict]): list of fertilizers

    Returns:
        [dict]: Two best fertilizers for pk equilibrium (2+ if ex-aequo)
    """
    
    target_p_percentage_on_pk = p / (p + k)
    fertilizers_with_p_percentage_on_pk = [
        add_fertilizer_p_percentage_on_pk(fertilizer)
        for fertilizer in fertilizers
    ]

    for fertilizer in fertilizers_with_p_percentage_on_pk:
        fertilizer['gap_p_target'] = abs(
            fertilizer['p_percentage_on_pk'] - target_p_percentage_on_pk
        )

    second_smallest_gap = sorted(
        fertilizers_with_p_percentage_on_pk, 
        key=lambda x: x['gap_p_target']
    )[1]['gap_p_target']

    fertilizers_with_best_pk_equilibrium = [
        fertilizer
        for fertilizer in fertilizers_with_p_percentage_on_pk
        if fertilizer['gap_p_target'] <= second_smallest_gap
    ]

    return fertilizers_with_best_pk_equilibrium


def get_fertilizer_with_best_npk_equilibrium(n: float,
                                             p: float, 
                                             k: float, 
                                             fertilizers: [dict]) -> dict:
    """Return fertilizer with the best npk equilibrium compared to a given 
        (n,p,k) triplet.

    Args:
        n (float): Target N
        p (float): Target P
        k (float): Target K
        fertilizers ([dict]): list of fertilizers

    Returns:
        dict: Best fertilizer for npk equilibrium
    """

    fertilizers_with_percentages_on_npk = [
        add_fertilizer_percentage_on_npk(fertilizer)
        for fertilizer in fertilizers
    ]

    for fertilizer in fertilizers_with_percentages_on_npk:
        fertilizer['gap_n_target'] = abs(
            fertilizer['n_percentage_on_npk'] - n
        )
        fertilizer['gap_p_target'] = abs(
            fertilizer['p_percentage_on_npk'] - p
        )
        fertilizer['gap_k_target'] = abs(
            fertilizer['k_percentage_on_npk'] - k
        )
        fertilizer['gap_targets'] = fertilizer['gap_n_target'] + \
                                        fertilizer['gap_p_target'] + \
                                        fertilizer['gap_k_target']
    
    best_fertilizer = sorted(
        fertilizers_with_percentages_on_npk, 
        key=lambda x: x['gap_targets']
    )[0]

    return best_fertilizer


def get_fertilizer_with_closest_n_share(n: float, 
                                        p:float, 
                                        k:float, 
                                        fertilizers: [dict]) -> dict:
    """Return fertilizer with closest N share among a list of fertilizers 
        compared with a (n,p,k) triplet. 

    Args:
        n (float): Nitrogen amount
        p (float): Phosphorus amount
        k (float): Potassium amount
        fertilizers ([dict]): List of fertilizers. Example :
        [
            {
                'id': 1,
                'name': '15-20-55',
                'registered_fertilizer_elements': [
                    {
                        'chemical_element_id': 'N', 
                        'percentage': 20, 
                        'efficiency': 100
                    }, {
                        'chemical_element_id': 'P', 
                        'percentage': 4.4, 
                        'efficiency': 100
                    }
                ]
            },
            {...}
        ]

    Returns:
        dict: Fertilizer with the closest N share
    """
    target_n_percentage_on_npk = n / (n + p + k)
    fertilizers_with_n_percentage_on_npk = [
        add_fertilizer_percentage_on_npk(fertilizer)
        for fertilizer in fertilizers
    ]

    for fertilizer in fertilizers_with_n_percentage_on_npk:
        fertilizer['gap_n_target'] = abs(
            fertilizer['n_percentage_on_npk'] - target_n_percentage_on_npk
        )
        
    fertilizer_with_closest_n_share = sorted(
        fertilizers_with_n_percentage_on_npk, 
        key=lambda x: x['gap_n_target']
    )[0]

    return fertilizer_with_closest_n_share

def compute_fertilizer_quantity_with_topdressing(p: float, 
                                                 k: float, 
                                                 fertilizer: dict) -> dict:
    """Compute the quantity of fertilizer needed for basal application with 
        topdressing according to P and K needs.
        The supply in N, P, K from this fertilizer quantity is also computed.

    Args:
        p (float): Phosphorus needs
        k (float): Potassium needs
        fertilizer (dict): A fertilizer with the following schemas :
            {
                'id': 1,
                'name': '15-20-55',
                'registered_fertilizer_elements': [
                    {
                        'chemical_element_id': 'N', 
                        'percentage': 20, 
                        'efficiency': 100
                    }, {
                        'chemical_element_id': 'P', 
                        'percentage': 4.4, 
                        'efficiency': 100
                    }
                ]
            }

    Returns:
        dict: The following keys : ['quantity', 'n_kg', 'p_kg', 'k_kg'] are 
            added to the fertilizer in input.
    """

    fertilizer_elements = fertilizer['registered_fertilizer_elements']
    n_percentage, p_percentage, k_percentage = [
        get_chemical_element_percentage_and_efficiency(
            registered_fertilizer_elements=fertilizer_elements,
            chemical_element_id=chemical_element_id
        )['percentage']
        for chemical_element_id in ['N', 'P', 'K']
    ]

    k_optimal = 0 if k_percentage == 0 else k / k_percentage
    p_optimal = 0 if p_percentage == 0 else p / p_percentage
    if p_percentage == 0:
        fertilizer['quantity'] = k_optimal
    elif k_percentage == 0:
        fertilizer['quantity'] = p_optimal
    else:
        p_ratio = p_optimal / (p_optimal + k_optimal)
        weighted_mean = p_optimal * p_ratio + k_optimal * (1 - p_ratio)
        fertilizer['quantity'] = weighted_mean

    fertilizer['n_kg'] = fertilizer['quantity'] * n_percentage
    fertilizer['p_kg'] = fertilizer['quantity'] * p_percentage
    fertilizer['k_kg'] = fertilizer['quantity'] * k_percentage

    return fertilizer


def compute_fertilizer_quantity_without_topdressing(n: float, 
                                                    fertilizer: dict) -> dict:
    """Compute the quantity of fertilizer needed for basal application without 
        topdressing according to N needs.
        The supply in N, P, K from this fertilizer quantity is also computed.

    Args:
        n (float): Nitrogen needs
        fertilizer (dict): A fertilizer with the following schemas :
            {
                'id': 1,
                'name': '15-20-55',
                'registered_fertilizer_elements': [
                    {
                        'chemical_element_id': 'N', 
                        'percentage': 20, 
                        'efficiency': 100
                    }, {
                        'chemical_element_id': 'P', 
                        'percentage': 4.4, 
                        'efficiency': 100
                    }
                ]
            }

    Returns:
        dict: The following keys : ['quantity', 'n_kg', 'p_kg', 'k_kg'] are 
            added to the fertilizer in input.
    """
    fertilizer_elements = fertilizer['registered_fertilizer_elements']
    n_percentage, p_percentage, k_percentage = [
        get_chemical_element_percentage_and_efficiency(
            registered_fertilizer_elements=fertilizer_elements,
            chemical_element_id=chemical_element_id
        )['percentage']
        for chemical_element_id in ['N', 'P', 'K']
    ]

    fertilizer['quantity'] = n / n_percentage 

    fertilizer['n_kg'] = fertilizer['quantity'] * n_percentage
    fertilizer['p_kg'] = fertilizer['quantity'] * p_percentage
    fertilizer['k_kg'] = fertilizer['quantity'] * k_percentage

    return fertilizer


def add_fertilizer_p_percentage_on_pk(compound_fertilizer: dict) -> dict:
    """Add phosphorus percentage over (phosphorus + potassium) for a given
    compound fertilizer.

    Args:
        compound_fertilizer (dict): A compound fertilizer object as dict 
            examples : {
                'id': 18, 
                'name': '10,5-52-0 (FOSFATO MONOAMONICO-MAP)', 
                'registered_fertilizer_elements': [
                    {
                        'chemical_element_id': 'N', 
                        'percentage': 10.5, 
                        'efficiency': 100
                    }, 
                    {
                        'chemical_element_id': 'P', 
                        'percentage': 22.88, 
                        'efficiency': 100
                    }
                ]
            }

    Returns:
        dict: The input dict with a new key 'p_percentage_on_pk' as a float
    """
    p, k = (0, 0)
    
    for element in compound_fertilizer['registered_fertilizer_elements']:
        if element['chemical_element_id'] == 'P':
            p = element['percentage']
        elif element['chemical_element_id'] == 'K':
            k = element['percentage']
    
    compound_fertilizer['p_percentage_on_pk'] = p / (p + k)

    return compound_fertilizer


def add_fertilizer_percentage_on_npk(compound_fertilizer: dict) -> dict:
    """Add phosphorus percentage over (phosphorus + potassium) for a given
    compound fertilizer.

    Args:
        compound_fertilizer (dict): A compound fertilizer object as dict 
            examples : {
                'id': 18, 
                'name': '10,5-52-0 (FOSFATO MONOAMONICO-MAP)', 
                'registered_fertilizer_elements': [
                    {
                        'chemical_element_id': 'N', 
                        'percentage': 10.5, 
                        'efficiency': 100
                    }, 
                    {
                        'chemical_element_id': 'P', 
                        'percentage': 22.88, 
                        'efficiency': 100
                    }
                ]
            }

    Returns:
        dict: The input dict with a new key 'p_percentage_on_pk' as a float
    """
    n, p, k = (0, 0, 0)
    
    for element in compound_fertilizer['registered_fertilizer_elements']:
        if element['chemical_element_id'] == 'N':
            n = element['percentage']
        elif element['chemical_element_id'] == 'P':
            p = element['percentage']
        elif element['chemical_element_id'] == 'K':
            k = element['percentage']
    
    compound_fertilizer['n_percentage_on_npk'] = n / (n + p + k)
    compound_fertilizer['p_percentage_on_npk'] = p / (n + p + k)
    compound_fertilizer['k_percentage_on_npk'] = k / (n + p + k)

    return compound_fertilizer

