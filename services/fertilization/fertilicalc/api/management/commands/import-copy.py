import csv
from psycopg2 import sql
from django.db import connections
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Import data using Postgres COPY statement.'

    def add_arguments(self, parser):
        parser.add_argument(
            'db_table',
            type=str,
            help='Database table to import data into.')
        parser.add_argument(
            '--db-alias',
            type=str,
            default='default',
            help='Django database alias to use (default=\'default\').')
        parser.add_argument(
            'csv_file',
            type=str,
            help='CSV file to import')
        parser.add_argument(
            '--csv-delimiter',
            type=str,
            default=',',
            help='CSV separator used to seprate values (default=\',\').')

    def handle(self, *args, **kwargs):
        db_alias = kwargs['db_alias']
        db_table = kwargs['db_table']
        csv_file = kwargs['csv_file']
        csv_delimiter = kwargs['csv_delimiter']

        with open(csv_file) as file:
            reader = csv.reader(file, delimiter=csv_delimiter)
            headers = next(reader)
            with connections[db_alias].cursor() as cursor:
                file.seek(0)
                cursor.copy_expert(
                    sql.SQL(
                        "COPY public.{}({}) FROM STDIN DELIMITER ',' CSV HEADER"
                        .format(db_table, ",".join(headers))),
                    file,
                )
