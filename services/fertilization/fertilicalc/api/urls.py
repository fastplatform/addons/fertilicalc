from django.urls import path
from . import views


urlpatterns = [
    path('algo/', views.fertilicalc_algo),
    path('handler_hasura_action_npk/', views.handler_hasura_action_npk),
    path('action_handler_get_best_compound_fertilizer_with_topdressing/', 
         views.action_handler_get_best_compound_fertilizer_with_topdressing),
    path('action_handler_get_best_compound_fertilizer_without_topdressing/', 
         views.action_handler_get_best_compound_fertilizer_without_topdressing),
    path('action_handler_generate_results_pdf/', views.action_handler_generate_results_pdf),
    path('fertilicalc_param/', views.get_fertilicalc_param),
]