from django.contrib import admin
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from constance.admin import ConstanceAdmin, ConstanceForm, Config

from .models import PhosphorusThreshold, PotassiumThreshold
from .models import BSoilEffect
from .models import BEfficiencyFactor
from .models import PlantSpeciesProfile

from .forms import *

from fertilicalc.plan import *
from fertilicalc.result import *

class MyAdminSite(admin.AdminSite):

    site_title  = 'FertiliCalc Administration'
    index_title = 'FertiliCalc Administration'

    def get_urls(self):
        from django.urls import path
        urls = super().get_urls()
        urls += [
            path('sandbox/', self.admin_view(self.sandbox_view))
        ]
        return urls

    def sandbox_view(self, request):

        # Construire le formulaire, soit avec les données postées,
        # soit vide si l'utilisateur accède pour la première fois
        # à la page.
        if request.method == 'POST':
            p_threshold_form = PhosphorusThresholdForm(request.POST, prefix='p_threshold')
            k_threshold_form = PotassiumThresholdForm(request.POST, prefix='k_threshold')
            soil_effect_form = SoilEffectForm(request.POST, prefix='soil_effect')
            efficiency_factor_form = EfficiencyFactorForm(request.POST, prefix='efficiency_factor')
            crop_concentration_form = PlantSpeciesProfileForm(request.POST, prefix='crop_concentration')
            n_equation_param_form = NEquationParameterForm(request.POST, prefix='n_equation_param')
            max_p_rate_form = MaxPRateForm(request.POST, prefix='max_p_rate')
            max_k_rate_form = MaxKRateForm(request.POST, prefix='max_k_rate')
            soil_form = SoilForm(request.POST, prefix='soil')
            soil_sample_form = SoilSampleForm(request.POST, prefix='sample')
            rotation_form = RotationForm(request.POST, prefix='rotation')
            strategy_form = StrategyForm(request.POST, prefix='strategy')
            if (p_threshold_form.is_valid() and k_threshold_form.is_valid() 
                and soil_effect_form.is_valid() and soil_form.is_valid() 
                and efficiency_factor_form.is_valid()
                and crop_concentration_form.is_valid() 
                and n_equation_param_form.is_valid()
                and max_p_rate_form.is_valid() and max_k_rate_form.is_valid()
                and soil_sample_form.is_valid() and rotation_form.is_valid() 
                and strategy_form.is_valid()):

                rotation = rotation_form.cleaned_data
                content = {
                    "rotation": [{
                        "crop_yield": rotation["crop_yield"],
                        "burn_residues": rotation["burn_residues"],
                        "crop_features": crop_concentration_form.cleaned_data
                    }],
                    "soil": soil_form.cleaned_data,
                    "sample": soil_sample_form.cleaned_data,
                    "p_threshold": p_threshold_form.cleaned_data,
                    "k_threshold": k_threshold_form.cleaned_data,
                    "soil_effect": soil_effect_form.cleaned_data,
                    "efficiency_factor": efficiency_factor_form.cleaned_data,
                    "max_p_rate": max_p_rate_form.cleaned_data,
                    "max_k_rate": max_k_rate_form.cleaned_data,
                    "n_equation_parameter": n_equation_param_form.cleaned_data,
                    "strategy": strategy_form.cleaned_data
                    }

                nmp = Plan.from_dict(content)

                msg = ""
                for id_rotation in range(len(RecommendationSet(nmp).requirements)):
                    npk = RecommendationSet(nmp).requirements[id_rotation]
                    msg += "\n-----------------------------"
                    msg += "\nRotation " + str(id_rotation)
                    msg += "\n-----------------------------"
                    msg += "\nN : " + str(npk["n"])
                    msg += "\nP : " + str(npk["p"])
                    msg += "\nK : " + str(npk["k"])

                envoi = True
        else:
            p_threshold_form = PhosphorusThresholdForm(prefix='p_threshold')
            k_threshold_form = PotassiumThresholdForm(prefix='k_threshold')
            soil_effect_form = SoilEffectForm(prefix='soil_effect')
            efficiency_factor_form = EfficiencyFactorForm(prefix='efficiency_factor')
            crop_concentration_form = PlantSpeciesProfileForm(prefix='crop_concentration')
            n_equation_param_form = NEquationParameterForm(prefix='n_equation_param')
            max_p_rate_form = MaxPRateForm(prefix='max_p_rate')
            max_k_rate_form = MaxKRateForm(prefix='max_k_rate')
            soil_form = SoilForm(prefix='soil')
            soil_sample_form = SoilSampleForm(prefix='sample')
            rotation_form = RotationForm(prefix='rotation')
            strategy_form = StrategyForm(prefix='strategy')

            envoi = False

        return render(request, 'api/sandbox.html', locals())


class ConfigAdmin(ConstanceAdmin):
    pass


class MyConstanceConfig(Config):
    class Meta(Config.Meta):
        verbose_name_plural = _('Fertilicalc algorithm parameters')
    _meta = Meta()


class ThresholdAdmin(admin.ModelAdmin):
    list_display = ('id', 'strategy', 'soil_type', 'value')
    ordering = ('strategy', 'soil_type',)


class BEfficiencyFactorAdmin(admin.ModelAdmin):
    list_display = ('id', 'strategy', 'soil_type', 'factor')
    ordering = ('strategy', 'soil_type',)


class BSoilEffectAdmin(admin.ModelAdmin):
    list_display = ('soil_type', 'coeff')
    ordering = ('soil_type',)


class PlantSpeciesProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'profile_name', 'harvest_product', 'dry_matter', 
                    'n', 'p', 'k')
    ordering = ('profile_name',)


admin_site = MyAdminSite()

# Register your models here.
admin_site.register(PhosphorusThreshold, ThresholdAdmin)
admin_site.register(PotassiumThreshold, ThresholdAdmin)
admin_site.register(BSoilEffect, BSoilEffectAdmin)
admin_site.register(BEfficiencyFactor, BEfficiencyFactorAdmin)
admin_site.register(PlantSpeciesProfile, PlantSpeciesProfileAdmin)
admin_site.register([MyConstanceConfig], ConfigAdmin)

admin_site.index_template = "admin/api/index.html"