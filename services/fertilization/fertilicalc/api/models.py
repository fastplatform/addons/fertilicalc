from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator


def _get_choice_verbose_name_for_given_choice_id(choices_tuple, choice_id):
    "Returns verbose name of the choice according to its id"

    for choice in enumerate(choices_tuple):
        if choice[1][0] == choice_id:
            return choice[1][1]


def _get_choice_id_for_given_choice_name(choices_tuple, choice_name):
    "Returns id of the choice according to its name"

    for choice in enumerate(choices_tuple):
        if choice[1][1] == choice_name:
            return choice[1][0]


class PlantSpeciesGroup(models.TextChoices):
    CEREALS = 'CEREALS', _('Cereals')
    FORAGE_NON_LEGUME = 'FORAGE_NON_LEGUME', _('Forage (non legume)')
    INDUSTRIAL = 'INDUSTRIAL', _('Industrial')
    PULSES = 'PULSES', _('Pulses')
    HORTICULTURAL = 'HORTICULTURAL', _('Horticultural')
    FORAGE_LEGUME = 'FORAGE_LEGUME', _('Forage (legume)')
    TUBERS_ROOT = 'TUBERS_ROOT', _('Tubers root')
    TREES = 'TREES', _('Trees')
    OTHER = 'OTHER', _('Other')


class Strategy(models.TextChoices):
    SUFFICIENCY = 'SUFFICIENCY', _('Sufficiency strategy (minimum fertilizer)')
    REDUCED = 'REDUCED', _('Build-up & maintenance (reduced fertilizer)')
    MAXIMUM = 'MAXIMUM', _('Build-up & maintenance (maximum yield)')
    MAINTENANCE = 'MAINTENANCE', _('Maintenance (soil analysis not available)')

class SoilType(models.TextChoices):
    SANDY = 'SANDY', _('Sandy')
    SANDY_LOAM = 'SANDY_LOAM', _('Sandy loam')
    LOAM = 'LOAM', _('Loam')
    SILTY_LOAM = 'SILTY_LOAM', _('Silty loam')
    CLAY_LOAM = 'CLAY_LOAM', _('Clay loam')
    CLAY = 'CLAY', _('Clay')


# Create your models here.
# models.py
class NutrientThreshold(models.Model):


    strategy = models.CharField(
        choices=Strategy.choices, 
        verbose_name=_('Strategy'),
        max_length=32
    )

    soil_type = models.CharField(
        choices=SoilType.choices, 
        verbose_name=_('Soil type'),
        max_length=32
    )

    value = models.PositiveSmallIntegerField(verbose_name=_('Threshold'))
    
    class Meta:
        abstract = True


class PhosphorusThreshold(NutrientThreshold):

    def __str__(self):
        return _('Phosphorus threshold for {strategy} and {soil_type} soil').format(
            strategy=_get_choice_verbose_name_for_given_choice_id(Strategy.choices, self.strategy),
            soil_type=_get_choice_verbose_name_for_given_choice_id(SoilType.choices, self.soil_type))

    class Meta:
        db_table = 'fertilicalc__phosphorus_threshold'
        verbose_name = _('Phosphorus threshold')
        verbose_name_plural = _('Phosphorus thresholds')
        abstract = False
        unique_together = ('strategy', 'soil_type', )


class PotassiumThreshold(NutrientThreshold):

    def __str__(self):
        return _('Potassium threshold for {strategy} and {soil_type} soil').format(
            strategy=_get_choice_verbose_name_for_given_choice_id(Strategy.choices, self.strategy),
            soil_type=_get_choice_verbose_name_for_given_choice_id(SoilType.choices, self.soil_type))

    class Meta:
        db_table = 'fertilicalc__potassium_threshold'
        verbose_name = _('Potassium threshold')
        verbose_name_plural = _('Potassium thresholds')
        abstract = False
        unique_together = ('strategy', 'soil_type', )


class AbstractSoilEffect(models.Model):

    soil_type = models.CharField(
        choices=SoilType.choices, 
        verbose_name=_('Soil type'),
        max_length=32,
        primary_key=True
    )

    coeff = models.FloatField(verbose_name=_('Coefficient'))

    def __str__(self):
        return _('Soil effect for {soil_type} soil').format(
            soil_type=_get_choice_verbose_name_for_given_choice_id(SoilType.choices, self.soil_type))
    
    class Meta:
        abstract = True
        verbose_name = _('Soil effect on P, K requirements')
        verbose_name_plural = _('Soil effects on P, K requirements')


class BSoilEffect(AbstractSoilEffect):

    class Meta:
        db_table = 'fertilicalc__soil_effect' 


class AbstractEfficiencyFactor(models.Model):

    strategy = models.CharField(
        choices=Strategy.choices, 
        verbose_name=_('Strategy'),
        max_length=32
    )

    soil_type = models.CharField(
        choices=SoilType.choices, 
        verbose_name=_('Soil type'),
        max_length=32
    )

    factor = models.FloatField(verbose_name=_('Efficiency factor'))


    def __str__(self):
        return _('Efficiency factor for {strategy} and {soil_type} soil').format(
            strategy=_get_choice_verbose_name_for_given_choice_id(Strategy.choices, self.strategy),
            soil_type=_get_choice_verbose_name_for_given_choice_id(SoilType.choices, self.soil_type))


    class Meta:
        abstract = True


class BEfficiencyFactor(AbstractEfficiencyFactor):

    class Meta:
        db_table = "fertilicalc__efficiency_factor"
        verbose_name = _('Potassium efficiency factor')
        verbose_name_plural = _('Potassium efficiency factors')
        unique_together = ('strategy', 'soil_type', )


class PlantSpeciesProfile(models.Model):

    plant_species_id = models.CharField(
        null=False,
        blank=False,
        max_length=50,
        verbose_name=_('plant species id'))
    profile_name = models.CharField(
        max_length=255,
        verbose_name=_('profile name'),
        blank=False, 
        null=False, 
        unique=True)
    harvest_product = models.CharField(
        max_length=255, 
        verbose_name=_('Harvest product'),
        blank=True, null=True)
    dry_matter = models.FloatField(
        verbose_name=_('Dry matter (%)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        blank=True, null=True)
    n = models.FloatField(
        verbose_name=_('Typical N concentration (% dry matter)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        blank=True, null=True)
    p = models.FloatField(
        verbose_name=_('P concentration (% dry matter)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        blank=True, null=True)
    k = models.FloatField(
        verbose_name=_('K concentration (% dry matter)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        blank=True, null=True)
    res_product = models.CharField(
        max_length=255, 
        verbose_name=_('Residue product'),
        blank=True, null=True)
    res_dry_matter = models.FloatField(
        blank=True,
        null=True,
        verbose_name=_('Dry matter in residues (%)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)])
    res_n = models.FloatField(
        blank=True,
        null=True,
        verbose_name=_('Typical N concentrationin residues'),
        validators=[MinValueValidator(0), MaxValueValidator(100)])
    res_p = models.FloatField(
        blank=True,
        null=True,
        verbose_name=_('P concentration in residues (% dry matter)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)])
    res_k = models.FloatField(
        blank=True,
        null=True,
        verbose_name=_('K concentration in residues (% dry matter)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)])
    nfix_code = models.BooleanField(
        verbose_name=_('N fixation'), blank=True, null=True)
    n_min = models.FloatField(
        blank=True,
        null=True,
        verbose_name=_('Minimal N concentration'),
        validators=[MinValueValidator(0), MaxValueValidator(100)])
    n_max = models.FloatField(
        blank=True,
        null=True,
        verbose_name=_('Maximal N concentration'),
        validators=[MinValueValidator(0), MaxValueValidator(100)])
    hi = models.FloatField(
        verbose_name=_('Harvest index (%)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        blank=True, null=True)
    f_res = models.FloatField(
        verbose_name=_('Fraction of residues left in field (%)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        blank=True, null=True)
    n_basal = models.FloatField(
        verbose_name=_('Basal application (%)'),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        blank=True, null=True)
    basal_description = models.CharField(
        max_length=2048, 
        verbose_name=_('Basal application description'),
        blank=True, null=True)
    post_planting_count = models.PositiveSmallIntegerField(
        verbose_name=_('Number of post-planting application'),
        blank=True, null=True)
    post_planting_description = models.CharField(
        max_length=2048, 
        verbose_name=_('Post planting applications description'),
        blank=True, null=True)
    default_low_yield = models.FloatField(
        verbose_name=_('Low yield default value'),
        blank=True,
        null=True
    )
    default_medium_yield = models.FloatField(
        verbose_name=_('Medium yield default value'),
        blank=True,
        null=True
    )
    default_high_yield = models.FloatField(
        verbose_name=_('High yield default value'),
        blank=True,
        null=True
    )
    sd_water_allocation_volume = models.FloatField(
        verbose_name=_('Standardized water allocation volume (m3)'),
        blank=True,
        null=True
    )
    
    def __str__(self):
        return self.profile_name
    
    class Meta:
        db_table = 'fertilicalc__plant_species_profile'
        verbose_name = _('plant species profile')
        verbose_name_plural = _('plant species profiles')
