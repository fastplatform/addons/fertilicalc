from django import forms
from constance import config

from .models import (
    PhosphorusThreshold, PotassiumThreshold, BSoilEffect, BEfficiencyFactor, 
    PlantSpeciesProfile, SoilType, Strategy, PlantSpeciesGroup
)

from django.utils.translation import ugettext_lazy as _


class PhosphorusThresholdForm(forms.ModelForm):
    class Meta:
        model = PhosphorusThreshold
        fields = ['value']

    def validate_unique(self):
        pass


class PotassiumThresholdForm(forms.ModelForm):
    class Meta:
        model = PotassiumThreshold
        fields = ['value']
    
    def validate_unique(self):
        pass


class SoilEffectForm(forms.ModelForm):
    class Meta:
        model = BSoilEffect
        fields = ['coeff']
    
    def validate_unique(self):
        pass


class EfficiencyFactorForm(forms.ModelForm):
    class Meta:
        model = BEfficiencyFactor
        fields = ['factor']
    
    def validate_unique(self):
        pass


class PlantSpeciesProfileForm(forms.ModelForm):

    plant_species_group = forms.CharField(
        widget=forms.Select(choices=PlantSpeciesGroup.choices),
        label=_('Plant species group')
    )
    class Meta:
        model = PlantSpeciesProfile
        exclude = [
            'plant_species_id', 'profile_name', 'n_basal', 'basal_description', 
            'post_planting_count', 'post_planting_description',
            'default_low_yield', 'default_medium_yield', 'default_high_yield',
            'sd_water_allocation_volume'
        ]
    
    def validate_unique(self):
        pass


class SoilForm(forms.Form):
    soil_type = forms.CharField(
        widget=forms.Select(choices=SoilType.choices), label=_('Soil type')
    )


class SoilSampleForm(forms.Form):
    p_conc = forms.IntegerField(initial=0, label=_('Phosphorus concentration'))
    k_conc = forms.IntegerField(initial=0, label=_('Potassium concentration'))
    som = forms.FloatField(initial=0.0, label=_('Soil organic matter'))


class RotationForm(forms.Form):
    crop_yield = forms.IntegerField(initial=2000, label=_('Yield'))
    burn_residues = forms.BooleanField(
        initial=False, required=False, label=_('Burn residues')
    )


class StrategyForm(forms.Form):
    strategy = forms.CharField(
        widget=forms.Select(choices=Strategy.choices), label=_('Strategy')
    )
    tillage = forms.BooleanField(initial=False, required=False, label=_('Tillage'))


class NEquationParameterForm(forms.Form):
    n_end = forms.IntegerField(initial=config.N_END, label=_('N end'))
    n_other = forms.IntegerField(initial=config.N_OTHER, label=_('N other'))
    n_lost = forms.FloatField(initial=config.N_LOST, label=_('N lost'))
    f_nr = forms.FloatField(initial=config.F_NR, label=_('F nr'))
    beta_pl = forms.FloatField(initial=config.BETA_PL, label=_('Beta PL'))
    efic = forms.FloatField(initial=config.EFIC, label=_('Efic'))


class MaxPRateForm(forms.Form):
    rate = forms.IntegerField(initial=config.MAX_P_RATE, label=_('Rate'))


class MaxKRateForm(forms.Form):
    rate = forms.IntegerField(initial=config.MAX_K_RATE, label=_('Rate'))