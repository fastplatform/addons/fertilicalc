from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import HttpResponseForbidden
from django.conf import settings
from django.templatetags.static import static

import json
import os
from constance import config
import requests
import base64

from fastplatform_graphql.hasura import HasuraClient

from fertilicalc.plan import Plan
from fertilicalc.result import RecommendationSet

from api.utils import get_all_fertilicalc_parameters, add_application_details
from api.fertilization.strategy import (
    get_best_compound_fertilizer_with_topdressing,
    get_best_compound_fertilizer_without_topdressing
)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))

API_GATEWAY_SERVICE_KEY = os.environ.get(
    'API_GATEWAY_SERVICE_KEY', 
    'default-service-key'
)

HASURA_HEADERS = {
    'Authorization': f'Service {API_GATEWAY_SERVICE_KEY}'
}

def get_fertilicalc_param(request):

    fertilicalc_param = {
        "max_p_rate": config.MAX_P_RATE,
        "max_k_rate": config.MAX_K_RATE,
        "n_end": config.N_END,
        "n_other": config.N_OTHER,
        "n_lost": config.N_LOST,
        "f_nr": config.F_NR,
        "beta_pl": config.BETA_PL,
        "efic": config.EFIC
    }

    return HttpResponse(json.dumps(fertilicalc_param, indent=4))


@csrf_exempt
def fertilicalc_algo(request):

    if request.method == 'POST':

        data = json.loads(request.body)

        nmp = Plan.from_dict(data)

        nmp_result = RecommendationSet(nmp)
        data["recommendations"] = nmp_result.requirements

        return HttpResponse(json.dumps(data, indent=4))

    else:
        print('no')
        return HttpResponse(str(request.method))


@csrf_exempt
@require_http_methods(['POST'])
def handler_hasura_action_npk(request):
    # We receive a JSON with 2 roots:
    # - session_variables: the request headers forwarded by Hasura
    # - input: a JSON representation of the input
    body =json.loads(request.body)
    inputs = body['input']['inputs']

    # If no prooper role, reject
    if 'x-hasura-role' not in body['session_variables']:
        return HttpResponseForbidden()
    if body['session_variables']['x-hasura-role'] not in ['farmer', 'service', 'admin']:
        return HttpResponseForbidden()

    all_recommendations = []
    for plot in inputs:

        sorted_rotations = sorted(plot['rotations'], key=lambda k: k['index']) 

        fertilicalc_params = get_all_fertilicalc_parameters(
            soil_type=plot['soil']['soil_type'], 
            strategy=plot['strategy'], 
            plant_species_profile_id=[
                rotation['profile'] 
                for rotation in sorted_rotations
            ],
            plant_species_groups=[
                rotation['plant_species_group'] 
                for rotation in sorted_rotations
            ],
            fraction_residues=[
                (
                    rotation['f_residues']
                    if 'f_residues' in rotation
                    else None
                )
                for rotation in sorted_rotations
            ]
        )

        plan = get_plan(
            crop_features=fertilicalc_params['plant_species_profile'], 
            rotations=clean_rotations(sorted_rotations), 
            soil=plot['soil'], 
            fertilicalc_params=fertilicalc_params, 
            strategy=plot['strategy'], 
            tillage=plot['tillage']
        )

        # Compute
        recommendations = RecommendationSet(plan).requirements

        for idx in range(len(sorted_rotations)):
            plot_id = plot['plot_id']
            rotation_id = sorted_rotations[idx]['index']

            recommendations[idx]['plot_id'] = plot_id
            recommendations[idx]['rotation_index'] = rotation_id
            recommendations[idx] = add_application_details(
                recommendation=recommendations[idx],
                application=fertilicalc_params['application'][idx]
            )
        
        all_recommendations.append(recommendations)

    flat_recommendations = [
        rotation_recommendation
        for plot_recommendations in all_recommendations
        for rotation_recommendation in plot_recommendations
    ]
    
    return HttpResponse(json.dumps(flat_recommendations))


def get_plan(
    crop_features, rotations, soil, fertilicalc_params, 
    strategy, tillage
):

    plan_input = {
        "rotation": [
            {
                "crop_yield": rotations[idx_rotation]["crop_yield"],
                "burn_residues": rotations[idx_rotation]["burn_residues"],
                "crop_features": crop_features[idx_rotation]
            }
            for idx_rotation in range(len(rotations))
        ],
        "soil": {
            "soil_type": soil['soil_type']
        },
        "sample": {
            "p_conc": soil['p'],
            "k_conc": soil['k'],
            "som": soil['som'],
            "cec":None,
            "ph":None
        },
        "p_threshold": {"value": fertilicalc_params["phosphorus_threshold"]},
        "k_threshold": {"value": fertilicalc_params["potassium_threshold"]},
        "soil_effect": {"coeff": fertilicalc_params["soil_effect"]},
        "efficiency_factor": {"factor": fertilicalc_params["efficiency_factor"]},
        "max_p_rate": {"rate": config.MAX_P_RATE},
        "max_k_rate": {"rate": config.MAX_K_RATE},
        "n_equation_parameter": {
                "n_end": config.N_END,
                "n_other": config.N_OTHER,
                "n_lost": config.N_LOST,
                "f_nr": config.F_NR,
                "beta_pl": config.BETA_PL,
                "efic": config.EFIC
        },
        "strategy": {
            "strategy": strategy,
            "tillage": tillage
        }
    }

    plan = Plan.from_dict(plan_input)

    return plan


def clean_rotations(rotations):

    cleaned_rotations = [
        {
            "crop_yield": rotation['yield'],
            "burn_residues": (
                rotation['collect_residues']
                if 'collect_residues' in rotation
                else False
            )
        }
        for rotation in rotations
    ]

    return cleaned_rotations


@csrf_exempt
@require_http_methods(['POST'])
def action_handler_get_best_compound_fertilizer_with_topdressing(request):
    # We receive a JSON with 2 roots:
    # - session_variables: the request headers forwarded by Hasura
    # - input: a JSON representation of the input
    body = json.loads(request.body)
    npk = body['input']['input']

    # If no prooper role, reject
    if 'x-hasura-role' not in body['session_variables']:
        return HttpResponseForbidden()
    if body['session_variables']['x-hasura-role'] not in ['farmer', 'service', 'admin']:
        return HttpResponseForbidden()
    
    hasura = HasuraClient("fastplatform", headers=HASURA_HEADERS)
    result = get_best_compound_fertilizer_with_topdressing(
        hasura=hasura, 
        n=npk['n'], 
        p=npk['p'], 
        k=npk['k']
    )

    return HttpResponse(json.dumps(result))


@csrf_exempt
@require_http_methods(['POST'])
def action_handler_get_best_compound_fertilizer_without_topdressing(request):
    # We receive a JSON with 2 roots:
    # - session_variables: the request headers forwarded by Hasura
    # - input: a JSON representation of the input
    body = json.loads(request.body)
    npk = body['input']['input']

    # If no prooper role, reject
    if 'x-hasura-role' not in body['session_variables']:
        return HttpResponseForbidden()
    if body['session_variables']['x-hasura-role'] not in ['farmer', 'service', 'admin']:
        return HttpResponseForbidden()
    
    hasura = HasuraClient("fastplatform", headers=HASURA_HEADERS)
    result = get_best_compound_fertilizer_without_topdressing(
        hasura=hasura, 
        n=npk['n'], 
        p=npk['p'], 
        k=npk['k']
    )

    return HttpResponse(json.dumps(result))

@csrf_exempt
@require_http_methods(['POST'])
def action_handler_generate_results_pdf(request):
    try:
        inputs = json.loads(request.body)
        inputs = inputs['input']['inputs']
        # read the results html template
        with open(CURRENT_DIR + '/templates/pdf/base.html', 'r') as f:
            template = f.read()
        try:
            # get algo logo as static image
            algo_logo_static_url = request.build_absolute_uri(static('logo_fertilicalc.png'))
            res_algo = requests.get(algo_logo_static_url)
            inputs['algorithm_logo_base64'] = 'data:image/png;base64,' + base64.b64encode(res_algo.content).decode()
            inputs['algorithm_name'] = settings.ALGORITHM_NAME
        except Exception as e:
            print('FAILED TO GET ALGORITHM LOGO', str(e))
        # generate PDF
        res = requests.post(settings.PDF_GENERATOR_ENDPOINT, json={
            "template": template,
            "data": inputs 
        })
        if (res.status_code == 200):
            try:
                base64_content = base64.b64encode(res.content).decode()
                return HttpResponse(json.dumps({'pdf': base64_content}))
            except Exception as e:
                print('Error while base64 encoding pdf content: {}'.format(str(e)))
                raise Exception(e)
        else:
            raise Exception('REQUEST TO PDF GENERATOR FAILED')
    except Exception as e:
        print('[ERROR] PDF generation error: {}'.format(str(e)))
        response = HttpResponse(json.dumps({
            "message": "error while generating results pdf",
            "code": "500"
        }))
        response.status_code = 400
        return response