from api.models import (
    _get_choice_id_for_given_choice_name, Strategy, SoilType, BSoilEffect,
    BEfficiencyFactor, PhosphorusThreshold, PotassiumThreshold,
    PlantSpeciesProfile
)


def filter_dict(dict_to_filter, keys):
    return { key: dict_to_filter[key] for key in keys }

def extract_profile_and_application_repartition(profile_row, 
                                                profile_keys, 
                                                application_keys):

    profile = filter_dict(dict_to_filter=profile_row, keys=profile_keys)
    application = filter_dict(dict_to_filter=profile_row, keys=application_keys)

    return {'profile': profile, 'application': application}

def get_all_fertilicalc_parameters(
    soil_type, strategy, plant_species_profile_id, plant_species_groups, fraction_residues
):

    efficiency_factor = BEfficiencyFactor.objects.get(
        soil_type=soil_type, 
        strategy=strategy
    )

    phosphorus_threshold = PhosphorusThreshold.objects.get(
        soil_type=soil_type, 
        strategy=strategy
    )
    potassium_threshold = PotassiumThreshold.objects.get(
        soil_type=soil_type, 
        strategy=strategy
    )
    soil_effect = BSoilEffect.objects.get(soil_type=soil_type)

    if isinstance(plant_species_profile_id, int):
        plant_species_profile_id = [plant_species_profile_id]
    
    profile_keys_to_keep = [
        'dry_matter', 'f_res', 'harvest_product', 'hi', 'k', 'n', 'n_max', 
        'n_min', 'nfix_code', 'p', 'res_dry_matter', 'res_k', 'res_n', 
        'res_p', 'res_product'
    ]
    application_keys = [
        'n_basal', 'basal_description', 'post_planting_count', 
        'post_planting_description'
    ]


    plant_species_profile = [
        extract_profile_and_application_repartition(
            profile_row=PlantSpeciesProfile.objects.get(id=id).__dict__, 
            profile_keys=profile_keys_to_keep,
            application_keys=application_keys
        )
        for id in plant_species_profile_id
    ]
    profile = [profile['profile'] for profile in plant_species_profile]

    application = [profile['application'] for profile in plant_species_profile]

    nb_rotations = len(plant_species_groups)
    for rotation_idx in range(nb_rotations):
        group = plant_species_groups[rotation_idx]
        profile[rotation_idx]["plant_species_group"] = group
        if fraction_residues[rotation_idx] is not None:
            profile[rotation_idx]['f_res'] = fraction_residues[rotation_idx]

    return {
        "efficiency_factor": efficiency_factor.factor,
        "phosphorus_threshold": phosphorus_threshold.value,
        "potassium_threshold": potassium_threshold.value,
        "soil_effect": soil_effect.coeff,
        "plant_species_profile": profile,
        "application": application
    }

def add_application_details(recommendation, application):

    n = recommendation['n']

    if (application['n_basal'] is not None) \
        & (application['post_planting_count'] is not None):
        n_basal = n * application['n_basal'] /100
        post_planting_count = application['post_planting_count']
        n_each_post_planting = (
            0 if post_planting_count == 0
            else (n - n_basal) / post_planting_count
        )
    else:
        n_basal = None
        post_planting_count = None
        n_each_post_planting= None

    basal_desc = application['basal_description']
    post_planting_desc = application['post_planting_description']

    recommendation['n_basal'] = n_basal
    recommendation['basal_description'] = basal_desc
    recommendation['n_each_post_planting'] = n_each_post_planting
    recommendation['post_planting_count'] = post_planting_count
    recommendation['post_planting_description'] = post_planting_desc

    return recommendation