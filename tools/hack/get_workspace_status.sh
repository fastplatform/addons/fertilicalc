#!/bin/bash

git_rev=$(git rev-parse HEAD 2>/dev/null)
echo "BUILD_SCM_REVISION ${git_rev}"

git diff-index --quiet HEAD -- 2>/dev/null
if [[ $? == 0 ]];
then
    tree_status="Clean"
else
    tree_status="Modified"
fi
echo "BUILD_SCM_STATUS ${tree_status}"

GITSHA=$(git describe --always 2>/dev/null)

FERTILICALC_VERSION=${FERTILICALC_VERSION:=$(grep 'FERTILICALC_VERSION\s*=' VERSION | awk '{print $3}')}
FERTILICALC_CI_VERSION=${FERTILICALC_CI_VERSION:=$(grep 'FERTILICALC_CI_VERSION\s*=' VERSION | awk '{print $3}')}

if [[ -z "${VERSION}" ]]; then
  if [[ -z "${CI}" ]]; then
    VERSION=${FERTILICALC_VERSION}
  else
    VERSION="${FERTILICALC_CI_VERSION}+${GITSHA}"
  fi
fi

echo "STABLE_FERTILICALC_VERSION ${VERSION}"

FERTILICALC_TAG=${VERSION/+/-}
echo "STABLE_FERTILICALC_TAG ${FERTILICALC_TAG}"
echo "STABLE_FERTILICALC_TAG_PREFIXED_WITH_COLON :${FERTILICALC_TAG}"

if [[ -z "${DOCKER_REGISTRY}" ]]; then
  DOCKER_REGISTRY="pwcfasteu.azurecr.io"
fi
if [[ -z "${DOCKER_IMAGE_PREFIX}" ]]; then
  DOCKER_IMAGE_PREFIX=fastplatform/addons/fertilicalc/
fi
echo "STABLE_DOCKER_REGISTRY ${DOCKER_REGISTRY}"
echo "STABLE_DOCKER_IMAGE_PREFIX ${DOCKER_IMAGE_PREFIX}"
