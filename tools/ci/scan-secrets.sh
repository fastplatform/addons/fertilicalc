#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/../hack/ensure-detect-secrets.sh
source $(dirname $0)/common.sh

bazel build //tools:jq
jq_bin=$(bazel info bazel-genfiles)/tools/jq

exclude_files_regex="\
secrets.env|\
tools/ci/envs/.*"

# Scan repository 
detect-secrets scan \
    --exclude-files $exclude_files_regex \
    --base64-limit 4.5 \
    --hex-limit 3.0 \
    --update .secrets-baseline

findings=$(detect-secrets audit .secrets-baseline --display-result | $jq_bin '.stats.unknowns.count')

if [ ! "${findings}" -eq "0" ]; then
  echo "[+] secrets may have been detected !"
  echo "[+] please, audit findings and remove the secrets (or update the baseline for false positive)"
  echo "[+] -> make scan-secrets"
  echo "[+] -> make audit-secrets"
  echo "[+] exiting with error ..."
  exit 1
fi

echo "[+] no secret found !"
